import pandas as pd
import os
from config import PROFILE_NAME, MAIN_STREET_NAME, PRIME_NAME


def main():
    profile = pd.read_csv(os.path.join('data', PROFILE_NAME))
    main = pd.read_csv(os.path.join('data', MAIN_STREET_NAME))
    prime = pd.read_csv(os.path.join('data', PRIME_NAME))


if __name__ == '__main__':
    main()
