"""
This scripts download the single year zip files from the Bureau of Labor
Statistic.  Then it process the files to clean up table header and filter the
data base on the project study area before importing into the database.
https://www.bls.gov/cew/downloadable-data-files.htm
"""

import pandas as pd
from sqlalchemy import create_engine


def filter_data(df):
    """ Helper function that filter the data based on study area """
    return df.loc[df["area_fips"].isin(COUNTIES)]


""" Basic parameter for study area and year range """
STATE_CODE = "17"
COUNTY_CODE = ["019", "041", "053", "075", "147", "183"]
COUNTIES = [int(f"{STATE_CODE}{c}") for c in COUNTY_CODE]
YEARS = range(2009, 2018)

if __name__ == "__main__":
    engine = create_engine(
        "{}://{}:{}@{}:{}/{}".format(
            "postgresql", "admin", "password", "localhost", "5432", "db",
        )
    )
    """ Drop existing tables if exists """
    conn = engine.connect()
    STATEMENT = "DROP TABLE IF EXISTS workforce.qcew;"
    conn.execute(STATEMENT)

    """ Download data for the specified year range """
    available_years = [str(y) for y in YEARS]
    for year in available_years:
        link = f"https://data.bls.gov/cew/data/files/{year}/csv/{year}_annual_singlefile.zip"
        print(link)
        df = pd.read_csv(link, compression="zip")
        df = filter_data(df)
        df.to_sql(
            "qcew", engine, "workforce", if_exists="append", chunksize=100000
        )
