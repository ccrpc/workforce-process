# Load Raw data into temp database

import pandas as pd
from sqlalchemy import create_engine

CSV_PATH = 'data/master_consolidated_warn_data.csv'

engine = create_engine(
    "{}://{}:{}@{}:{}/{}".format(
        "postgresql", "admin", "password", "localhost", "5432", "db",
    )
)


def main():
    df = pd.read_csv(CSV_PATH)
    columns = [
        'company_name',
        'county',
        'event_type',
        'first_layoff_date',
        'number_workers_affected',
        'naics',
        'month',
        'year'
    ]
    counties = ['Champaign', 'Douglas', 'Ford', 'Iroquois', 'Piatt', 'Vermilion']
    df.columns = columns
    df = df[df['county'].isin(counties)]
    df.to_sql('warn', engine, schema='workforce', if_exists="replace")


if __name__ == '__main__':
    main()
