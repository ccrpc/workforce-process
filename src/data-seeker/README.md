# Data Extractions
This directionary contains the scripts that are used to extract data from various federal data sources and load them into database and transformed.

## Data Sources
These are the external data sources being used.

| Source | Product | Indicator |
| --- | --- | --- | 
| U.S. Census Bureau | American Community Survey | Households |
| U.S. Census Bureau | American Community Survey | Household Income |
| U.S. Census Bureau | Quarterly Workforce Indicator | Employment |
| U.S. Census Bureau | Quarterly Workforce Indicator | Turnover Rate |
| U.S. Census Bureau | Quarterly Workforce Indicator | Firm Job Change |
| U.S. Census Bureau | Quarterly Workforce Indicator | Average Monthly Earnings |
| U.S. Bureau of Labor Statistic | Quarterly Census of Employment and Wages | Businesses |
| U.S. Bureau of Labor Statistic | Quarterly Census of Employment and Wages | Total Wages |
| U.S. Bureau of Labor Statistic | Quarterly Census of Employment and Wages | Average Annual Pay |
| U.S. Bureau of Labor Statistic | Local Area Unemployment Statistics | Unemployment |
| U.S. Bureau of Labor Statistic | Local Area Unemployment Statistics | Labor Force |
| IDES | Labor Force | Labor Force |
| IDES | Warn | Layoff numbers |


## American Community Survey (ACS)
The data uses the American Community Survey 5 years estimate.

| Variable | Code |
| --- | --- |
| Total household | B10063_001E |
| Household income | B19019_001E |

## Quarterly Workforce Indicator (QWI)
| Variable | Code | Usage |
| --- | --- | --- |
| Employment | Emp | variable | 
| Firm job change | FrmJbC | variable | 
| Earnings | EarnS | variable |
| Turn over rate | TurnOvrS | variable |
| Hires All (Stable) | HirAs | reaggregation |
| Seperations (Stable), Next Quarter | SepSnx | reaggregation | 
| Full-Quarter Employment (Stable) |EmpS | reaggregation |

## Local Area Unemployment Statistics (LAUS)
| Variable | Code |
| --- | --- |
| Labor Force | labor_force |
| Unemployed | unemployed |

## Quarterly Census of Employment and Wages (QCEW)
| Variable | Code |
| --- | --- |
| Annaul Average Establishments | annual_avg_estabs |
| Total Annual Wages | total_annual_wages |
| Average Annaul Pay | avg_annual_pay |
| Annual Average Employment Level | annual_avg_emplvl | 

### Importing to the database
The U.S. Census Bureau has an REST API that allows developer to access the ACS data directly.  The `acs_api.py` and `qwi_api.py` leverage this method and downloaded the data specifically for our workforce area and only indicators that the project is interested in. Statistic from BLS are donwloaded from Excels and Zip files and then processed before importing into the database using `laus_dump.py` and `qcew_dump.py`.


### Encoding the data for the workforce data portal
This process is filter out the unnecessary data and to enocde the data for more efficient transfer between the backend to the workforce data portal application.

The data are encoded into a 5 charater string that will yearly data associate to the encoded value.  

| var | 2009 | 2010 | 2011 |
| --- | --- | --- | --- |
| CD0MC | 2986 | 2334 | 2340 |
| CD0FC | 2098 | 2108 | 2147 |

- The 1st character of the string is the **Variable**.
- The 2nd character of the string is the **County**.
- The 3rd character of the string is the **Industry**.
- The 4th character of the string is the **Worker Type**.
- The 5th character of the string is the **Business Type**.

See the following for the explanation for each value.

##### Variable Code
| code | description |
| --- | --- |
| C | frmjbc | 
| E | emp_count |
| T | trnovers |
| R | earnings |
| U | unemployed |
| L | labor_force |
| S | annual_avg_establs |
| A | avg_annual_pay | 
| P | total_annual_pay |
| H | total_household |
| I | household_median_income |

##### County Code
| code | description | GeoID |
| --- | --- | --- | --- |
| C | Champaign County | 019 |
| D | Douglas County | 041 |
| F | Ford County | 053 |
| I | Iroquois County | 075 |
| P | Piatt County | 147 |
| V | Vermilion County | 183 |

##### Industry Code
| code | description |
| --- | --- |
| 1 | 11 |
| 2 | 21 |
| 3 | 22 |
| 4 | 23 |
| 5 | 31-33 |
| 6 | 42 |
| 7 | 44-45 |
| 8 | 48-49 |
| 9 | 51 |
| 0 | 52 |
| A | 53 |
| B | 54 |
| C | 55 |
| D | 56 |
| E | 61 |
| F | 62 |
| G | 71 |
| H | 72 |
| I | 81 |
| J | 92 |
| Z | All |

##### Worker Type Code
###### Sex
| code | description | original code |
| --- | --- | --- |
| M | Male | 1 |
| F | Female | 2 |

###### Race
| code | description | original code |
| --- | --- | --- |
| W | White | A1 |
| B | Black or African American Alone | A2 |
| N | American Indian or Alaska Native Alone | A3 |
| A | Asian Alone | A4 |
| P | Native Hawaiian or Other Pacific Islander Alone | A5 |
| T | Two or more race groups | A7 |

###### Ethnicity
| code | description | original code |
| --- | --- | --- |
| H | Hispanic | A1 |
| K | Non-Hispanic | A2 |

##### Business Type Code
###### Ownership
| code | description | original code |
| --- | --- | --- |
| C | State and local government plus private ownership | A00 |
| P | Private | A05 |

- A00 = [5, 3, 2]
- A05 = [5]

###### Size
| code | description | original code |
| --- | --- | --- |
| 0 | All Firm Sizes | 0 |
| 1 | 0-19 Employees | 1 |
| 2 | 20-49 Employees | 2 |
| 3 | 50-249 Employees | 3 |
| 4 | 250-499 Employees | 4 |
| 5 | 500+ Employees | 5 |
