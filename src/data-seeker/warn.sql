-- WARN SQL
CREATE TEMP TABLE warn AS
(
  SELECT
    county,
    SUM(number_workers_affected::integer) number_workers_affected,
    naics,
    year
  FROM (
    SELECT
      county,
      number_workers_affected,
      CASE
        WHEN naics BETWEEN 31 AND 33 THEN '31'
        WHEN naics BETWEEN 44 AND 45 THEN '44'
        WHEN naics BETWEEN 48 AND 49 THEN '48'
        ELSE naics
      END AS naics,
      year
    FROM workforce.warn
  ) AS naics_group
  GROUP BY county, year, naics
  ORDER BY county, year
);

CREATE TEMP TABLE warn_final AS 
WITH warn_piv AS (
SELECT
	'W' 
  ||
	CASE
    WHEN county = 'Champaign' THEN 'C'
    WHEN county = 'Douglas' THEN 'D'
    WHEN county = 'Ford' THEN 'F'
    WHEN county = 'Iroquois' THEN 'I'
    WHEN county = 'Piatt' THEN 'P'
    WHEN county = 'Vermilion' THEN 'V'
  END 
	||
  CASE
    WHEN naics = 11 THEN '0'
    WHEN naics = 21 THEN '1'
    WHEN naics = 22 THEN '2'
    WHEN naics = 23 THEN '3'
    WHEN naics = 31 THEN '4'
    WHEN naics = 42 THEN '5'
    WHEN naics = 44 THEN '6'
    WHEN naics = 48 THEN '7'
    WHEN naics = 51 THEN '8'
    WHEN naics = 52 THEN '9'
    WHEN naics = 53 THEN 'A'
    WHEN naics = 54 THEN 'B'
    WHEN naics = 55 THEN 'C'
    WHEN naics = 56 THEN 'D'
    WHEN naics = 61 THEN 'E'
    WHEN naics = 62 THEN 'F'
    WHEN naics = 71 THEN 'G'
    WHEN naics = 72 THEN 'H'
    WHEN naics = 81 THEN 'I'
    WHEN naics = 92 THEN 'J'
  END
  ||
  'ZZ' AS variable,
  year,
  number_workers_affected AS value
FROM warn
)
SELECT 
    variable,
    MAX(value) FILTER (WHERE year='2010') AS "2010",
    MAX(value) FILTER (WHERE year='2011') AS "2011",
    MAX(value) FILTER (WHERE year='2012') AS "2012",
    MAX(value) FILTER (WHERE year='2013') AS "2013",
    MAX(value) FILTER (WHERE year='2014') AS "2014",
    MAX(value) FILTER (WHERE year='2015') AS "2015",
    MAX(value) FILTER (WHERE year='2016') AS "2016",
    MAX(value) FILTER (WHERE year='2017') AS "2017",
    MAX(value) FILTER (WHERE year='2018') AS "2018",
    MAX(value) FILTER (WHERE year='2019') AS "2019",
    MAX(value) FILTER (WHERE year='2020') AS "2020"
FROM warn_piv
GROUP BY variable;

-- Export table to CSV
COPY (
  SELECT * FROM warn_final WHERE variable IS NOT NULL
) TO '/tmp/warn.csv' CSV HEADER;