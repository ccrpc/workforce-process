import pandas as pd
import numpy as np

columns = [
    "var",
    "val",
    "county",
    "industry",
    "worker",
    "business",
    "2009",
    "2010",
    "2011",
    "2012",
    "2013",
    "2014",
    "2015",
    "2016",
    "2017",
    "2018",
    "2019",
]
show_columns = [
    "var",
    "2009",
    "2010",
    "2011",
    "2012",
    "2013",
    "2014",
    "2015",
    "2016",
    "2017",
    "2018",
    "2019",
]


# test_df = pd.DataFrame(test_data)
# df = pd.read_csv("data/qwi_process.csv", header=None)
# df.columns = show_columns
# df = pd.DataFrame(
#     {
#         "A": ["foo", "bar", "foo", "bar", "foo", "bar"],
#         "B": [1, 2, 3, 4, 5, 6],
#         "C": [2.0, 5.0, 8.0, 1.0, 2.0, 9.0],
#     }
# )
# df = df.sort_values("A")
# import pdb

# pdb.set_trace()


def aggregate(df, columns, show_columns):
    var = ["val", "county", "industry", "worker", "business"]
    df["val"] = df["var"].apply(lambda x: x[0])
    df["county"] = df["var"].apply(lambda x: x[1])
    df["industry"] = df["var"].apply(lambda x: x[2])
    df["worker"] = df["var"].apply(lambda x: x[3])
    df["business"] = df["var"].apply(lambda x: x[4])

    # Need to group by single group of value such as Business size and type
    business_type = df.loc[df["business"].isin(["C", "P"])]
    business_type = (
        business_type.groupby(["val", "county", "industry", "worker"])
        .sum()
        .reset_index()
    )
    business_size = df.loc[df["business"].isin(["C", "P"])]
    business_size = (
        business_size.groupby(["val", "county", "industry", "worker"])
        .sum()
        .reset_index()
    )
    business_type["business"] = "Z"
    business_size["business"] = "Z"
    df = df.append([business_type, business_size], sort=True)[columns]
    df = df.groupby(var).first().reset_index()

    # Need to group by single group of value such as Sex, Race, or Ethnicity
    sex = df.loc[df["worker"].isin(["F", "M"])]
    sex = (
        sex.groupby(["val", "county", "industry", "business"])
        .sum()
        .reset_index()
    )
    race = df.loc[df["worker"].isin(["W", "B", "N", "A", "P", "T"])]
    race = (
        race.groupby(["val", "county", "industry", "business"])
        .sum()
        .reset_index()
    )
    ethnicity = df.loc[df["worker"].isin(["H", "K"])]
    ethnicity = (
        ethnicity.groupby(["val", "county", "industry", "business"])
        .sum()
        .reset_index()
    )
    sex["worker"] = "Z"
    race["worker"] = "Z"
    ethnicity["worker"] = "Z"
    df = df.append([sex, race, ethnicity], sort=True)[columns]
    df = df.groupby(var).first().reset_index()

    # Industry
    industry = df.groupby(["val", "county", "worker", "business"]).sum()
    industry = industry.reset_index()
    industry["industry"] = "Z"
    df = df.append(industry, sort=True)[columns]
    df = df.groupby(var).first().reset_index()

    county = df.groupby(["val", "industry", "worker", "business"]).sum()
    county = county.reset_index()
    county["county"] = "Z"
    df = df.append(county, sort=True)[columns]
    df = df.groupby(var).first().reset_index()

    df["var"] = (
        df["val"]
        + df["county"]
        + df["industry"]
        + df["worker"]
        + df["business"]
    )
    df = df[show_columns]

    # deduplication
    df = df.groupby("var").first()
    df = df.replace(0, np.nan)
    return df


def run_test():
    show_columns = ["var", "2009"]
    columns = [
        "var",
        "val",
        "county",
        "industry",
        "worker",
        "business",
        "2009",
    ]

    def single(columns, show_columns):
        # Aggregating county
        county_data = {"var": ["CCZZZ", "CDZZZ"], "2009": [1, 2]}
        county_df = pd.DataFrame.from_dict(county_data)
        result = aggregate(county_df, columns, show_columns)
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {"var": ["CCZZZ", "CDZZZ", "CZZZZ"], "2009": [1, 2, 3]}
            ).set_index("var"),
        )
        print("Aggregating County")
        print(result)
        print("-------------------")

        # Aggregating Industry
        ind_data = {"var": ["CC1ZZ", "CC2ZZ"], "2009": [1, 2]}
        ind_df = pd.DataFrame.from_dict(ind_data)
        result = aggregate(ind_df, columns, show_columns)
        print("Aggregating Industry")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CC1ZZ",
                        "CC2ZZ",
                        "CCZZZ",
                        "CZ1ZZ",
                        "CZ2ZZ",
                        "CZZZZ",
                    ],
                    "2009": [1, 2, 3, 1, 2, 3],
                }
            ).set_index("var"),
        )

        # Aggregating Worker Type
        worker_data = {"var": ["CCZMZ", "CCZFZ"], "2009": [3, 5]}
        worker_df = pd.DataFrame.from_dict(worker_data)
        result = aggregate(worker_df, columns, show_columns)
        print("Aggregating Worker")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CCZFZ",
                        "CCZMZ",
                        "CCZZZ",
                        "CZZFZ",
                        "CZZMZ",
                        "CZZZZ",
                    ],
                    "2009": [5, 3, 8, 5, 3, 8],
                }
            ).set_index("var"),
        )

        # Aggregating Firm Type
        firm_data = {"var": ["CCZZC", "CCZZP"], "2009": [3, 5]}
        firm_df = pd.DataFrame.from_dict(firm_data)
        result = aggregate(firm_df, columns, show_columns)
        print("Aggregating Firm")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CCZZC",
                        "CCZZP",
                        "CCZZZ",
                        "CZZZC",
                        "CZZZP",
                        "CZZZZ",
                    ],
                    "2009": [3, 5, 8, 3, 5, 8],
                }
            ).set_index("var"),
        )

    single(columns, show_columns)

    def two(columns, show_columns):
        # Aggregating County, Industry
        ci_data = {
            "var": ["CC1ZZ", "CC2ZZ", "CD1ZZ", "CD2ZZ"],
            "2009": [2, 3, 5, 7],
        }
        ci_df = pd.DataFrame.from_dict(ci_data)
        result = aggregate(ci_df, columns, show_columns)
        print("Aggregating County, Industry")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CC1ZZ",
                        "CC2ZZ",
                        "CCZZZ",
                        "CD1ZZ",
                        "CD2ZZ",
                        "CDZZZ",
                        "CZ1ZZ",
                        "CZ2ZZ",
                        "CZZZZ",
                    ],
                    "2009": [2, 3, 5, 5, 7, 12, 7, 10, 17],
                }
            ).set_index("var"),
        )

        # Aggregating County, Worker
        cw_data = {
            "var": ["CCZMZ", "CCZFZ", "CDZMZ", "CDZFZ"],
            "2009": [2, 3, 5, 7],
        }
        cw_df = pd.DataFrame.from_dict(cw_data)
        result = aggregate(cw_df, columns, show_columns)
        print("Aggregating County, Industry")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CCZFZ",
                        "CCZMZ",
                        "CCZZZ",
                        "CDZFZ",
                        "CDZMZ",
                        "CDZZZ",
                        "CZZFZ",
                        "CZZMZ",
                        "CZZZZ",
                    ],
                    "2009": [3, 2, 5, 7, 5, 12, 10, 7, 17],
                }
            ).set_index("var"),
        )

        # Aggregating County, Worker with multiple type
        cw_data = {
            "var": ["CCZMZ", "CCZFZ", "CCZAZ", "CCZWZ", "CCZBZ"],
            "2009": [7, 11, 3, 7, 8],
        }
        cw_df = pd.DataFrame.from_dict(cw_data)
        result = aggregate(cw_df, columns, show_columns)
        print("Aggregating County, Industry")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CCZAZ",
                        "CCZBZ",
                        "CCZFZ",
                        "CCZMZ",
                        "CCZWZ",
                        "CCZZZ",
                        "CZZAZ",
                        "CZZBZ",
                        "CZZFZ",
                        "CZZMZ",
                        "CZZWZ",
                        "CZZZZ",
                    ],
                    "2009": [3, 8, 11, 7, 7, 18, 3, 8, 11, 7, 7, 18],
                }
            ).set_index("var"),
        )

        # Aggregating County, Worker with multiple type
        cw_data = {
            "var": ["CCZMZ", "CCZFZ", "CCZAZ", "CCZWZ", "CCZBZ"],
            "2009": [7, 11, 3, 7, 8],
        }
        cw_df = pd.DataFrame.from_dict(cw_data)
        result = aggregate(cw_df, columns, show_columns)
        print("Aggregating County, Worker with Multiple type")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CCZAZ",
                        "CCZBZ",
                        "CCZFZ",
                        "CCZMZ",
                        "CCZWZ",
                        "CCZZZ",
                        "CZZAZ",
                        "CZZBZ",
                        "CZZFZ",
                        "CZZMZ",
                        "CZZWZ",
                        "CZZZZ",
                    ],
                    "2009": [3, 8, 11, 7, 7, 18, 3, 8, 11, 7, 7, 18],
                }
            ).set_index("var"),
        )

        # Aggregating County, Business with multiple firm type
        cw_data = {
            "var": ["CCZZC", "CCZZP", "CCZZ1", "CCZZ2", "CCZZ3"],
            "2009": [7, 11, 3, 7, 8],
        }
        cw_df = pd.DataFrame.from_dict(cw_data)
        result = aggregate(cw_df, columns, show_columns)
        print("Aggregating County, Business with mulitple firm type")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CCZZ1",
                        "CCZZ2",
                        "CCZZ3",
                        "CCZZC",
                        "CCZZP",
                        "CCZZZ",
                        "CZZZ1",
                        "CZZZ2",
                        "CZZZ3",
                        "CZZZC",
                        "CZZZP",
                        "CZZZZ",
                    ],
                    "2009": [3, 7, 8, 7, 11, 18, 3, 7, 8, 7, 11, 18],
                }
            ).set_index("var"),
        )

    two(columns, show_columns)

    def multiple(columns, show_columns):
        # Aggregating Multiple Counties, Multiple Industries, Multiple Worker Type
        ci_data = {
            "var": [
                "CC1MZ",
                "CC1FZ",
                "CC2MZ",
                "CC2FZ",
                "CD1MZ",
                "CD1FZ",
                "CD2MZ",
                "CD2FZ",
            ],
            "2009": [2, 3, 5, 7, 3, 5, 7, 11],
        }
        ci_df = pd.DataFrame.from_dict(ci_data)
        result = aggregate(ci_df, columns, show_columns)
        print(
            "Aggregating Multiple Counties, Multiple Industries, Multiple Worker Type"
        )
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CC1FZ",
                        "CC1MZ",
                        "CC1ZZ",
                        "CC2FZ",
                        "CC2MZ",
                        "CC2ZZ",
                        "CCZFZ",
                        "CCZMZ",
                        "CCZZZ",
                        "CD1FZ",
                        "CD1MZ",
                        "CD1ZZ",
                        "CD2FZ",
                        "CD2MZ",
                        "CD2ZZ",
                        "CDZFZ",
                        "CDZMZ",
                        "CDZZZ",
                        "CZ1FZ",
                        "CZ1MZ",
                        "CZ1ZZ",
                        "CZ2FZ",
                        "CZ2MZ",
                        "CZ2ZZ",
                        "CZZFZ",
                        "CZZMZ",
                        "CZZZZ",
                    ],
                    "2009": [
                        3,
                        2,
                        5,
                        7,
                        5,
                        12,
                        10,
                        7,
                        17,
                        5,
                        3,
                        8,
                        11,
                        7,
                        18,
                        16,
                        10,
                        26,
                        8,
                        5,
                        13,
                        18,
                        12,
                        30,
                        26,
                        17,
                        43,
                    ],
                }
            ).set_index("var"),
        )

        # Aggregating Multiple Counties, Multiple Industries, Multiple Worker Type (mix)
        ci_data = {
            "var": [
                "CC1AZ",
                "CC1BZ",
                "CC1MZ",
                "CC1FZ",
                "CC2MZ",
                "CC2FZ",
                "CD1AZ",
                "CD1BZ",
                "CD1MZ",
                "CD1FZ",
                "CD2MZ",
                "CD2FZ",
            ],
            "2009": [1, 2, 2, 3, 5, 7, 9, 10, 3, 5, 7, 11],
        }
        ci_df = pd.DataFrame.from_dict(ci_data)
        result = aggregate(ci_df, columns, show_columns)
        print(
            "Aggregating Multiple Counties, Multiple Industries, Multiple Worker Type (Mix)"
        )
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CC1AZ",
                        "CC1BZ",
                        "CC1FZ",
                        "CC1MZ",
                        "CC1ZZ",
                        "CC2FZ",
                        "CC2MZ",
                        "CC2ZZ",
                        "CCZAZ",
                        "CCZBZ",
                        "CCZFZ",
                        "CCZMZ",
                        "CCZZZ",
                        "CD1AZ",
                        "CD1BZ",
                        "CD1FZ",
                        "CD1MZ",
                        "CD1ZZ",
                        "CD2FZ",
                        "CD2MZ",
                        "CD2ZZ",
                        "CDZAZ",
                        "CDZBZ",
                        "CDZFZ",
                        "CDZMZ",
                        "CDZZZ",
                        "CZ1AZ",
                        "CZ1BZ",
                        "CZ1FZ",
                        "CZ1MZ",
                        "CZ1ZZ",
                        "CZ2FZ",
                        "CZ2MZ",
                        "CZ2ZZ",
                        "CZZAZ",
                        "CZZBZ",
                        "CZZFZ",
                        "CZZMZ",
                        "CZZZZ",
                    ],
                    "2009": [
                        1,
                        2,
                        3,
                        2,
                        5,
                        7,
                        5,
                        12,
                        1,
                        2,
                        10,
                        7,
                        17,
                        9,
                        10,
                        5,
                        3,
                        8,
                        11,
                        7,
                        18,
                        9,
                        10,
                        16,
                        10,
                        26,
                        10,
                        12,
                        8,
                        5,
                        13,
                        18,
                        12,
                        30,
                        10,
                        12,
                        26,
                        17,
                        43,
                    ],
                }
            ).set_index("var"),
        )

    multiple(columns, show_columns)

    show_columns = ["var", "2009", "2010"]
    columns = [
        "var",
        "val",
        "county",
        "industry",
        "worker",
        "business",
        "2009",
        "2010",
    ]

    def multiple_year(columns, show_columns):
        # Multiple years
        ci_data = {
            "var": ["CC1ZZ", "CC2ZZ", "CD1ZZ", "CD2ZZ"],
            "2009": [2, 3, 5, 7],
            "2010": [3, 4, 6, 7],
        }
        ci_df = pd.DataFrame.from_dict(ci_data)
        result = aggregate(ci_df, columns, show_columns)
        print("Multiple years, Aggregating County, Industry")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CC1ZZ",
                        "CC2ZZ",
                        "CCZZZ",
                        "CD1ZZ",
                        "CD2ZZ",
                        "CDZZZ",
                        "CZ1ZZ",
                        "CZ2ZZ",
                        "CZZZZ",
                    ],
                    "2009": [2, 3, 5, 5, 7, 12, 7, 10, 17],
                    "2010": [3, 4, 7, 6, 7, 13, 9, 11, 20],
                }
            ).set_index("var"),
        )

        # Multiple years with no data
        ci_data = {
            "var": ["CC1ZZ", "CC2ZZ", "CD1ZZ", "CD2ZZ"],
            "2009": [2, 3, 5, 7],
            "2010": [None, None, None, None],
        }
        ci_df = pd.DataFrame.from_dict(ci_data)
        result = aggregate(ci_df, columns, show_columns)
        print("Multiple years with no data, Aggregating County, Industry")
        print(result)
        print("-------------------")
        assert pd.DataFrame.equals(
            result,
            pd.DataFrame(
                {
                    "var": [
                        "CC1ZZ",
                        "CC2ZZ",
                        "CCZZZ",
                        "CD1ZZ",
                        "CD2ZZ",
                        "CDZZZ",
                        "CZ1ZZ",
                        "CZ2ZZ",
                        "CZZZZ",
                    ],
                    "2009": [2, 3, 5, 5, 7, 12, 7, 10, 17],
                    "2010": [
                        np.NaN,
                        np.NaN,
                        np.NaN,
                        np.NaN,
                        np.NaN,
                        np.NaN,
                        np.NaN,
                        np.NaN,
                        np.NaN,
                    ],
                }
            ).set_index("var"),
        )

    multiple_year(columns, show_columns)


# run_test()


def process_acs():
    # process ACS
    df = pd.read_csv("data/acs.csv")
    df.columns = show_columns
    df = df.set_index("var")

    # aggregate dataframe
    adf = df.filter(regex="^H", axis=0).reset_index()

    # # non aggregated dataframe
    ndf = df.filter(regex="^I", axis=0).reset_index()

    cdf = aggregate(adf, columns, show_columns)
    final_df = cdf.reset_index().append(ndf)
    final_df.to_csv("./data/acs_processed.csv", index=False)


def process_laus():
    # process LAUS
    df = pd.read_csv("data/laus.csv")
    df.columns = show_columns

    final_df = aggregate(df, columns, show_columns)
    final_df.to_csv("./data/laus_processed.csv")


def process_qcew():
    df = pd.read_csv("data/qcew.csv")
    df.columns = show_columns
    df = df.set_index("var")
    # aggregate dataframe
    adf = df.filter(regex="^(S|P)", axis=0).reset_index()

    # # non aggregated dataframe
    ndf = df.filter(regex="^A", axis=0).reset_index()

    cdf = aggregate(adf, columns, show_columns)
    final_df = cdf.reset_index().append(ndf)
    final_df.to_csv("./data/qcew_processed.csv", index=False)


# process_acs()
# process_laus()
process_qcew()
