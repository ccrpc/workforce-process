"""
This scripts download the single year Excel files from the Bureau of Labor
Statistic.  Then it process the files to clean up table header and filter the
data base on the project study area before importing into the database.
"""
import pandas as pd
from sqlalchemy import create_engine


""" Getting the range of year, (09 - 19) """
YEARS = [str(i) if len(str(i)) == 2 else "0" + str(i) for i in range(9, 19)]


if __name__ == "__main__":
    engine = create_engine(
        "{}://{}:{}@{}:{}/{}".format(
            "postgresql", "admin", "password", "localhost", "5432", "db",
        )
    )

    """ Drop existing tables in the database if exists """
    conn = engine.connect()
    STATEMENT = "DROP TABLE IF EXISTS workforce.laus;"
    conn.execute(STATEMENT)

    """ Download Excels, parse and load into database """
    for year in YEARS:
        link = f"https://www.bls.gov/lau/laucnty{year}.xlsx"
        print(link)
        df = pd.read_excel(link)
        columns = [
            "code",
            "state",
            "county",
            "name",
            "year",
            "empty",
            "labor_force",
            "employed",
            "unemployed",
            "ynemployment_rate",
        ]
        df.columns = columns
        df = df.loc[5 : len(df.index) - 4]
        df = df.drop(["empty"], axis=1)
        il = df.set_index(["state", "county"]).loc[("17")].reset_index()
        il["State"] = "17"

        il.to_sql("laus", engine, schema="workforce", if_exists="append")
