import time
import warnings
import pandas as pd
import datetime
from sqlalchemy import create_engine
import urllib.request
from key import API_KEY

warnings.filterwarnings("ignore")
pd.set_option("display.max_rows", 500)


class API:
    """
    Baseclass for creating the Census class, this class has the template for
    downloading constructing HTTP query and downloading the data.
    """
    def __init__(self, key, baseUrl):
        """
        Constructor for the class

        Arguments:
        key -- string - API key for accessing the API
        baseUrl -- string - Base Url end point for the API
        """
        self.key = key
        self.baseUrl = baseUrl
        self.query = ""

    def construct_query(self):
        """
        Abstract method that needs to be implement by a child class
        """
        raise NotImplementedError("Implemnt by child class")

    def download_data(self, verbose=False):
        """
        Download data based on query constructed

        Keyword arguments:
        verbose -- boolean
        """
        self.construct_query(verbose)
        request = urllib.request.Request(self.query)
        return urllib.request.urlopen(request)


class Census(API):
    def __init__(self, key, baseUrl="https://api.census.gov/data"):
        """
        Constructor for the class

        Arguments:
        key -- string - API key for accessing the API
        baseUrl -- string - Base Url end point for the API
        year -- string - data year
        fields -- list - list of indicator to download
        filter -- list - list of filter parameter to apply to the query
        """
        super().__init__(key, baseUrl)
        self.year = ""
        self.survey = ""
        self.fields = []
        self.filter = []

    def get_API_KEY(self):
        """ Print API key """
        print(self.key)

    def set_year(self, year):
        """
        Set the year for download

        Arguments:
        year -- integer / string
        """
        self.year = str(year)

    def set_survey(self, survey):
        """
        Set the survey for download

        Arguments:
        survey -- string
        """
        self.survey = str(survey)

    def set_field(self, fields):
        """
        Set the fields for download

        Arguments:
        fields -- list (example: ['emp', 'year'])
        """
        self._checkIsList(fields)
        self.fields = []
        for field in fields:
            self.fields.append(field)

    def set_geoFilter(self, filter):
        """
        Set the filters to apply to download

        Arguments:
        filter -- list (example: [in=state:17]), see censsu for how filters 
            can be used.
        """
        self._checkIsList(filter)
        if type(filter) is not list:
            raise TypeError
        self.filter = []
        for criteria in filter:
            self.filter.append(criteria)

    def construct_query(self, verbose=False):
        """
        Construct query based on the object project

        Keyword Arguments:
        verbose -- boolean
        """
        fields = ",".join(self.fields)
        criteria = "&".join(self.filter)
        baseQuery = "{}/{}/{}?get={}&{}&key={}".format(
            self.baseUrl, self.year, self.survey, fields, criteria, self.key
        )
        self.query = baseQuery
        if verbose:
            print(self.query)

    def download_data(self, verbose=False):
        """
        Download the data

        Keyword Arguments:
        verbose -- boolean
        """
        self._checkIsEmpty()
        self.construct_query(verbose)
        request = urllib.request.Request(self.query)
        response = urllib.request.urlopen(request)
        return pd.read_json(response)

    def _checkIsEmpty(self):
        """
        Helper method to check the validity of the object
        """
        if self.year == "":
            raise ValueError("Year has not been set")
        elif self.survey == "":
            raise ValueError("Survey has not been set")
        elif self.fields == []:
            raise ValueError("Fields have not been set")
        elif self.set_geoFilter == []:
            raise ValueError("Geofilter has not been set")

    def _checkIsList(self, input):
        """
        Helper method to check the validity of the input
        """
        if type(input) is not list:
            raise TypeError(
                "The input of {} must be in a list format".format(str(input))
            )


def delay(seconds, message=None):
    """
    Add delay between API query

    Argument:
    seconds -- integer, how long is the delay

    Keyword Argument:
    message -- string, custom message to display during the wait period
    """
    if message:
        print(message)
    for i in range(seconds):
        time.sleep(1)
    print("---------")


def clean_data(df):
    """ Use the first row to column header """
    df = df.rename(df.loc[0], axis="columns")
    df = df.iloc[1:]
    return df


def data_structure():
    """ Create an empty dataframe with the correct column headers """
    cols = [
        "year",
        "time",
        "industry",
        "state",
        "county",
        "sex",
        "race",
        "ethnicity",
        "ownercode",
        "firmsize",
        "Emp",
        "TurnOvrS",
        "FrmJbC",
        "EarnS",
        "HirAs",
        "SepSnx",
        "EmpS",
    ]
    return pd.DataFrame(columns=cols)


def process(df):
    """ Force covert numeric string into numeric data """
    df = clean_data(df)
    df = df.apply(pd.to_numeric, errors="ignore")
    return df


def download_data(survey, census, variable, filters):
    """ Helper function for downloading the data """
    census.set_survey(survey)
    census.set_field([variable, "year"])
    census.set_geoFilter(filters)
    df = census.download_data(True)
    return df


def get_current_quarter(month):
    """ Helper method to calculate the current quarter based on current time"""
    return ((month - 1) // 3) + 1


"""
The following are variables and filters that needs to be set for our study area
"""
CURRENT_YEAR = datetime.datetime.now().year
CURRENT_QUARTER = get_current_quarter(datetime.datetime.now().month)
COUNTIES = ["019", "041", "053", "075", "147", "183"]
INDUSTRIES = [
    "11",
    "21",
    "22",
    "23",
    "31-33",
    "42",
    "44-45",
    "48-49",
    "51",
    "52",
    "53",
    "54",
    "55",
    "56",
    "61",
    "62",
    "71",
    "72",
    "81",
    "92",
]
SEXES = ["1", "2"]
OWNERSHIPS = ["A00", "A05"]
FIRMSIZES = ["1", "2", "3", "4", "5"]
RACES = ["A1", "A2", "A3", "A4", "A5", "A7"]
ETHNICITIES = ["A1", "A2"]
VARIABLES = ["Emp", "TurnOvrS", "FrmJbC", "EarnS", "HirAs", "SepSnx", "EmpS"]

YEAR_PARAM = f"time=from2009-Q1to2019-Q4"
COUNTY_PARAM = f"for=county:{','.join(COUNTIES)}"
STATE_PARAM = f"in=state:17"
INDUSTRY_PARAM = f"industry={'&industry='.join(INDUSTRIES)}"
SEX_PARAM = f"sex={'&sex='.join(SEXES)}"
OWNERSHIP_PARAM = f"ownercode={'&ownercode='.join(OWNERSHIPS)}"
FIRMSIZE_PARAM = f"ownercode=A05&firmsize={'&firmsize='.join(FIRMSIZES)}"
RACE_PARAM = f"race={'&race='.join(RACES)}"
ETHNICITIY_PARAM = f"ethnicity={'&ethnicity='.join(ETHNICITIES)}"
VARIABLE_PARAM = ",".join(VARIABLES)
engine = create_engine(
    "{}://{}:{}@{}:{}/{}".format(
        "postgresql", "admin", "password", "localhost", "5432", "db",
    )
)

if __name__ == "__main__":
    census = Census(API_KEY)
    census.set_year("timeseries")
    main_df = data_structure()

    """
    QWI has different endpoint for series of data SE and RH.
    The following of helper methods that connect to the different endpoint to
    download the data.  The following methods also breakdown queries into
    smaller pieces becase the Census REST API has a limit of maximum of
    400,000 cells per query.  The query that is larger than the limit will
    return an error.
    """
    def get_sexes():
        # get combination of industry, combination of sexes, and all ownership type
        delay(5)
        df = download_data(
            "qwi/se",
            census,
            VARIABLE_PARAM,
            [COUNTY_PARAM, STATE_PARAM, YEAR_PARAM, INDUSTRY_PARAM, SEX_PARAM],
        )
        df = process(df)
        df["ownercode"] = "all"
        return df

    def get_sexes_firmsize():
        df = pd.DataFrame()
        for county in COUNTIES:
            delay(5)
            ndf = download_data(
                "qwi/se",
                census,
                VARIABLE_PARAM,
                [
                    f"for=county:{county}",
                    STATE_PARAM,
                    YEAR_PARAM,
                    INDUSTRY_PARAM,
                    SEX_PARAM,
                    FIRMSIZE_PARAM,
                ],
            )
            ndf = process(ndf)
            df = df.append(ndf)
        return df

    def get_races():
        # get combination of industry, combination of races, and all ownership type
        delay(5)
        df = download_data(
            "qwi/rh",
            census,
            VARIABLE_PARAM,
            [
                COUNTY_PARAM,
                STATE_PARAM,
                YEAR_PARAM,
                INDUSTRY_PARAM,
                RACE_PARAM,
            ],
        )
        df = process(df)
        df["ownercode"] = "all"
        df["race"] = "r" + df["race"].astype(str)
        return df

    def get_races_firmsize():
        # get combination of industry, combination of races, and private / firmsize
        ndf = pd.DataFrame()
        for county in COUNTIES:
            delay(5)
            county_param = f"for=county:{county}"
            df = download_data(
                "qwi/rh",
                census,
                VARIABLE_PARAM,
                [
                    county_param,
                    STATE_PARAM,
                    YEAR_PARAM,
                    INDUSTRY_PARAM,
                    RACE_PARAM,
                    FIRMSIZE_PARAM,
                ],
            )
            df = process(df)
            ndf = pd.concat([ndf, df])
        ndf["race"] = "r" + ndf["race"].astype(str)
        return ndf

    def get_ethnicity():
        # get combination of industry, combination of ethncity, and all ownership type
        delay(5)
        df = download_data(
            "qwi/rh",
            census,
            VARIABLE_PARAM,
            [
                COUNTY_PARAM,
                STATE_PARAM,
                YEAR_PARAM,
                INDUSTRY_PARAM,
                ETHNICITIY_PARAM,
            ],
        )
        df = process(df)
        df["ownercode"] = "all"
        return df

    def get_ethnicity_firmsize():
        # get combination of industry, combination of ethncity, and all ownership type
        df = pd.DataFrame()
        for county in COUNTIES:
            delay(5)
            ndf = download_data(
                "qwi/rh",
                census,
                VARIABLE_PARAM,
                [
                    f"for=county:{county}",
                    STATE_PARAM,
                    YEAR_PARAM,
                    INDUSTRY_PARAM,
                    ETHNICITIY_PARAM,
                    FIRMSIZE_PARAM,
                ],
            )
            ndf = process(ndf)
            df = df.append(ndf)
        return df

    """ This aggregate all the data into single dataframe for export """
    main_df = pd.concat(
        [
            main_df,
            get_races(),
            get_races_firmsize(),
            get_sexes(),
            get_sexes_firmsize(),
            get_ethnicity(),
            get_ethnicity_firmsize(),
        ]
    )
    """ Convert data header to lower case and import into the database """
    main_df.columns = [column.lower() for column in main_df.columns]
    main_df.to_sql("qwi", engine, "workforce", if_exists="replace")
