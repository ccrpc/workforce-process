"""
Initial work for downloading Longitudinal Employer-Household Dynamics
"""

import requests
from bs4 import BeautifulSoup
import pandas as pd
from sqlalchemy import create_engine


def download_data(url, director=None):
    return requests.get(url).content


if __name__ == "__main__":
    conn = create_engine(
        "{}://{}:{}@{}:{}/{}".format(
            "postgresql", "admin", "password", "localhost", "5432", "db",
        )
    )
    latest_release = "https://lehd.ces.census.gov/data/lodes/LODES7/il/od/"
    latest_year = 2017
    res = requests.get(latest_release)

    soup = BeautifulSoup(res.text)

    for link in soup.findAll("a"):
        if f"{latest_year}.csv.gz" in link.get("href"):
            print(f'Getting {link.get("href")}...')
            data = download_data(f'{latest_release}{link.get("href")}')
            open(link.get("href"), "wb").write(data)
            chunk = pd.read_csv(
                link.get("href"), compression="gzip", chunksize=100000
            )
            for df in chunk:
                df.to_sql(
                    'lodes', conn, schema='workforce', if_exists="append"
                )
