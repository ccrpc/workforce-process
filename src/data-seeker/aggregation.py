import itertools
import csv
from collections import OrderedDict
import re
import numpy as np

"""
Goals:

1. Aggregation across the each index of the code [X]
2. Have logic that aggregates only certain value of the code (Sex, Race, Ethnicity) [X]
3. Aggregate using custom forumla with existing values [X]
4. Aggregate using custom forumla from values outside of this data structure [X]
5. Should avoid aggregating across ownercode [X]
6. Do not aggregate median income [X]
"""

group = {
    'A': ['A', '[CDFIPV]', '[A-Y0-9]', 'Z', '[CP]'],
    'P': ['P', '[CDFIPV]', '[A-Y0-9]', 'Z', '[CP]'],
    'S': ['S', '[CDFIPV]', '[A-Y0-9]', 'Z', '[CP]'],
    'C': ['C', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
    'E': ['E', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
    'H': ['H', '[CDFIPV]', 'Z', 'Z', 'Z'],
    'I': ['I', '[CDFIPV]', 'Z', 'Z', 'Z'],
    'L': ['L', '[CDFIPV]', 'Z', 'Z', 'Z'],
    'R': ['R', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
    'T': ['T', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
    'U': ['U', '[CDFIPV]', 'Z', 'Z', 'Z'],
    'W': ['W', '[CDFIPV]', '[A-Y0-9]', 'Z', 'Z'],
}

VARIABLE = ["A", "P", "S", "C", "E", "T", "R", "U", "L", "H", "I", 'W']

COUNTY = ["C", "D", "F", "I", "P", "V", "Z"]
INDUSTRY = [
    *[str(i) for i in range(10)],
    *[chr(x) for x in range(ord('A'), ord('J') + 1)],
    'Z'
]

WORKER = ["M", "F", "W", "B", "N", "A", "P", "T", "H", "K", "Z"]
BUSINESS = ["C", "P", *[str(i) for i in range(6)], "Z"]


def generate_permutation() -> [str]:
    """
    This generates a complete list of permutation avaiable based on all the
    codes available in our projects.
    """
    all_list = [VARIABLE, COUNTY, INDUSTRY, WORKER, BUSINESS]
    res = list(itertools.product(*all_list))
    res = ["".join(l) for l in res]
    return res


def open_data(in_path) -> OrderedDict:
    """
    Open Data File and convert it to dictionary.
    #? Don't think I need to use OrderedDict for this approach anymore.
    """
    with open(in_path) as csvfile:
        reader = csv.reader(csvfile, delimiter=",")
        data = OrderedDict()
        for index, row in enumerate(reader):
            if index == 0:
                continue

            data[row[0]] = row[1:]
        data = OrderedDict(sorted(data.items()))
    return data


def aggregate(data: dict, code: str) -> list:
    """
    Main function to perform the logic of aggregation, find the values of all
    subcodes and perform aggregation
    """
    if code[0] == 'A':
        return get_annual_average_pay(code, data)
    elif code[0] == 'T':
        return get_turn_over_rate(code, data)
    else:
        return get_simple_aggregation(code, data)


def construct_data_group(code: str) -> list:
    """
    Base on the code, find all the codes that need to be used to create the
    aggregated value.  Return the list of codes
    """
    """
    APS cannot be aggregated across business type because of reaggregation
    previously
    """
    group = {
        'A': ['A', '[CDFIPV]', '[A-Y0-9]', 'Z', '[CP]'],
        'P': ['P', '[CDFIPV]', '[A-Y0-9]', 'Z', '[CP]'],
        'S': ['S', '[CDFIPV]', '[A-Y0-9]', 'Z', '[CP]'],
        'C': ['C', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
        'E': ['E', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
        'H': ['H', '[CDFIPV]', 'Z', 'Z', 'Z'],
        'I': ['I', '[CDFIPV]', 'Z', 'Z', 'Z'],
        'L': ['L', '[CDFIPV]', 'Z', 'Z', 'Z'],
        'R': ['R', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
        'T': ['T', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
        'U': ['U', '[CDFIPV]', 'Z', 'Z', 'Z'],
        'M': ['M', '[CDFIPV]', '[A-Y0-9]', 'Z', '[CP]'],
        'Q': ['Q', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
        'Y': ['Y', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
        'X': ['X', '[CDFIPV]', '[A-Y0-9]', '[MF]', '[12345]'],
        'W': ['W', '[CDFIPV]', '[A-Y0-9]', 'Z', 'Z'],
    }

    code_array = []

    for (i, c) in enumerate(code):
        if c != 'Z':
            code_array.append(c)
        else:
            code_array.append(group[code[0]][i])
    return ''.join(code_array)


def get_simple_aggregation(code: str, data: dict) -> list:
    regex = construct_data_group(code)
    results = []
    aggregated_result = [0] * 11
    for k, v in data.items():
        if re.search(regex, k):
            results.append(v)

    if len(results) > 0:
        for result in results:
            for i, v in enumerate(result):
                if v == '':
                    continue
                aggregated_result[i] = aggregated_result[i] + float(v)
    return aggregated_result


def get_annual_average_pay(code: str, data: dict) -> list:
    # get the code 
    # Do not aggregate business size
    if code[4] == 'Z':
        return get_simple_aggregation(code[0:4] + 'C', data)

    pay_code = code.replace(code[0], "P", 1)
    emplvl_code = code.replace(code[0], "M", 1)

    pay_result = np.array(
        get_simple_aggregation(pay_code, data))
    emplvl_result = np.array(
        get_simple_aggregation(emplvl_code, data))

    result = np.round(np.nan_to_num(pay_result / emplvl_result), 2).tolist()

    return result


def get_turn_over_rate(code: str, data: dict) -> list:
    hire_code = code.replace(code[0], "Y", 1)
    sep_code = code.replace(code[0], "X", 1)
    emps_code = code.replace(code[0], "Q", 1)

    hire_result = np.array(
        get_simple_aggregation(hire_code, data))
    sep_result = np.array(
        get_simple_aggregation(sep_code, data))
    emps_result = np.array(
        get_simple_aggregation(emps_code, data))

    result = np.around(np.nan_to_num(
       (hire_result + sep_result) / (2 * emps_result)
    ), 2).tolist()
    return result


def remove_aggregation_data(data: dict) -> dict:
    filter_list = ['Q', 'X', 'Y', 'M']

    new_data = {}
    for key, value in data.items():
        if key[0] not in filter_list:
            new_data[key] = round_data(key, value)
    return new_data


def round_data(key: str, data: list) -> list:
    whole_number_list = ['C', 'E', 'R', 'U', 'L', 'S', 'A', 'P', 'H', 'I']
    # round to whole number
    if key[0] in whole_number_list:
        return [int(round(float(v))) if v != '' else None for v in data]
    # round to 2 decimal place
    else:
        return [round(float(v), 2) if v != '' else None for v in data]


if __name__ == "__main__":
    # data = open_data('data/combine.csv')
    data = open_data('data/warn.csv')
    final = {}
    for code in generate_permutation():
        if code in data:
            final[code] = data[code]
            continue
        if code not in data and 'Z' not in code:
            continue

        # skip aggregation for earning
        if code[0] == 'R':
            continue

        aggregated = aggregate(data, code)
        if aggregated != [0] * 11:
            final[code] = aggregated

    final = remove_aggregation_data(final)

    # with open('data/output.csv', 'w') as csvfile:
    with open('data/warn_output.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        # writer.writerow(['var', *[i for i in range(2009, 2019)]])
        writer.writerow(['var', *[i for i in range(2010, 2021)]])
        for k, v in final.items():
            writer.writerow([k, *v])
