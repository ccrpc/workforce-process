-- This is a script that transform and encode the data into a format for 
-- the workforce data portal application

-- ACS 
CREATE TEMP TABLE acs_size_filter AS (
  SELECT
    county,
    year,
    total_household AS value,
    'total_household' AS variable
  FROM workforce.acs
);

CREATE TEMP TABLE acs_income_filter AS (
  SELECT
    county,
    year,
    median_income AS value,
    'median_income' AS variable
  FROM workforce.acs 
);

CREATE TEMP TABLE acs_household_size AS (
  WITH acs_piv AS (
    SELECT
      -- encode total_household
      'H'
      ||
      CASE
        WHEN county = 19 THEN 'C'
        WHEN county = 41 THEN 'D'
        WHEN county = 53 THEN 'F'
        WHEN county = 75 THEN 'I'
        WHEN county = 147 THEN 'P'
        WHEN county = 183 THEN 'V'
      END
      ||
      'ZZZ'
      AS variable,
      year,
      value
    FROM acs_size_filter
    WHERE value IS NOT NULL
  )
  SELECT
    variable,
    NULL::FLOAT AS "2009",
    MAX(value) FILTER (WHERE year='2010') AS "2010",
    MAX(value) FILTER (WHERE year='2011') AS "2011",
    MAX(value) FILTER (WHERE year='2012') AS "2012",
    MAX(value) FILTER (WHERE year='2013') AS "2013",
    MAX(value) FILTER (WHERE year='2014') AS "2014",
    MAX(value) FILTER (WHERE year='2015') AS "2015",
    MAX(value) FILTER (WHERE year='2016') AS "2016",
    MAX(value) FILTER (WHERE year='2017') AS "2017",
    MAX(value) FILTER (WHERE year='2018') AS "2018",
    NULL::FLOAT AS "2019"
  FROM acs_piv
  GROUP BY variable
  ORDER BY variable
);

CREATE TEMP TABLE acs_median_income AS (
  WITH acs_piv AS (
    SELECT
      -- encode median income
      'I'
      ||
      CASE
        WHEN county = 19 THEN 'C'
        WHEN county = 41 THEN 'D'
        WHEN county = 53 THEN 'F'
        WHEN county = 75 THEN 'I'
        WHEN county = 147 THEN 'P'
        WHEN county = 183 THEN 'V'
      END
      ||
      'ZZZ'
      AS variable,
      year,
      value
    FROM acs_income_filter
    WHERE value IS NOT NULL
  )
  SELECT
    variable,
    NULL::FLOAT AS "2009",
    MAX(value) FILTER (WHERE year='2010') AS "2010",
    MAX(value) FILTER (WHERE year='2011') AS "2011",
    MAX(value) FILTER (WHERE year='2012') AS "2012",
    MAX(value) FILTER (WHERE year='2013') AS "2013",
    MAX(value) FILTER (WHERE year='2014') AS "2014",
    MAX(value) FILTER (WHERE year='2015') AS "2015",
    MAX(value) FILTER (WHERE year='2016') AS "2016",
    MAX(value) FILTER (WHERE year='2017') AS "2017",
    MAX(value) FILTER (WHERE year='2018') AS "2018",
    NULL::FLOAT AS "2019"
  FROM acs_piv
  GROUP BY variable
  ORDER BY variable
);

CREATE TEMP TABLE acs AS (
  SELECT * FROM acs_household_size
  UNION
  SELECT * FROM acs_median_income
);

-- LAUS
CREATE TEMP TABLE laus_combine AS (
  SELECT
      county, 
      year,
      labor_force AS value,
      'labor_force' AS variable
  FROM workforce.laus

  UNION

  SELECT
      county,
      year,
      unemployed AS value,
      'unemployed' AS variable
  FROM workforce.laus
);

CREATE TEMP TABLE laus AS (
  WITH laus_piv AS (
    SELECT
      -- encode variable
      CASE
          WHEN variable = 'unemployed' THEN 'U'
          WHEN variable = 'labor_force' THEN 'L'
      END
      ||
      -- encode county
      CASE
        WHEN county = '019' THEN 'C'
        WHEN county = '041' THEN 'D'
        WHEN county = '053' THEN 'F'
        WHEN county = '075' THEN 'I'
        WHEN county = '147' THEN 'P'
        WHEN county = '183' THEN 'V'
      END
      ||
      -- none value for industry, worker type and business type
      'ZZZ'
      AS variable,
      year,
      value
    FROM laus_combine
    WHERE VALUE IS NOT NULL
  )
  SELECT
    variable,
    MAX(value) FILTER (WHERE year='2009') AS "2009",
    MAX(value) FILTER (WHERE year='2010') AS "2010",
    MAX(value) FILTER (WHERE year='2011') AS "2011",
    MAX(value) FILTER (WHERE year='2012') AS "2012",
    MAX(value) FILTER (WHERE year='2013') AS "2013",
    MAX(value) FILTER (WHERE year='2014') AS "2014",
    MAX(value) FILTER (WHERE year='2015') AS "2015",
    MAX(value) FILTER (WHERE year='2016') AS "2016",
    MAX(value) FILTER (WHERE year='2017') AS "2017",
    MAX(value) FILTER (WHERE year='2018') AS "2018",
    NULL::FLOAT AS "2019"
  FROM laus_piv
  WHERE variable IS NOT NULL
  GROUP BY variable
  ORDER BY variable
);

-- QCEW
CREATE TEMP TABLE qcew_by_county AS (
  SELECT
      year,
      area_fips,
      own_code,
      size_code,
      industry_code,
      annual_avg_emplvl,
      avg_annual_pay,
      total_annual_wages,
      annual_avg_estabs
  FROM workforce.qcew
  WHERE
      (
          area_fips = '17019' OR
          area_fips = '17041' OR
          area_fips = '17053' OR
          area_fips = '17075' OR
          area_fips = '17147' OR
          area_fips = '17183'
      ) AND
      (
          LENGTH(industry_code) = 2 OR
          industry_code = '31-33' OR 
          industry_code = '44-45'
      )
);

CREATE TEMP TABLE qcew_all_owner AS (
  SELECT
    year,
    area_fips,
    industry_code,
    'A00' AS own_code,
    SUM(total_annual_wages) AS total_annual_wages,
    SUM(annual_avg_emplvl) AS annual_avg_emplvl,
    SUM(annual_avg_estabs) AS annual_avg_estabs
  FROM qcew_by_county
  WHERE (
    own_code = 5 OR own_code = 3 OR own_code = 2
  )
  GROUP BY
    year,
    area_fips,
    industry_code
);

CREATE TEMP TABLE qcew_all_private AS (
  SELECT
    year,
    area_fips,
    industry_code,
    'A05' AS own_code,
    SUM(total_annual_wages) AS total_annual_wages,
    SUM(annual_avg_emplvl) AS annual_avg_emplvl,
    SUM(annual_avg_estabs) AS annual_avg_estabs
  FROM qcew_by_county
  WHERE (
    own_code = 5
  )
  GROUP BY
    year,
    area_fips,
    industry_code
);


CREATE TEMP TABLE qcew_reaggregate_owner AS (
  SELECT * FROM qcew_all_owner
  WHERE annual_avg_emplvl <> 0
  UNION
  SELECT * FROM qcew_all_private
  WHERE annual_avg_emplvl <> 0
);


CREATE TEMP TABLE qcew_combine AS (
  SELECT
      year,
      area_fips,
      own_code,
      industry_code,
      'avg_annual_pay' as variable,
      (total_annual_wages / annual_avg_emplvl)::int as value
  FROM qcew_reaggregate_owner

  UNION

  SELECT
      year,
      area_fips,
      own_code,
      industry_code,
      'annual_avg_estabs' as variable,
      annual_avg_estabs as value
  FROM qcew_reaggregate_owner

  UNION

  SELECT
      year,
      area_fips,
      own_code,
      industry_code,
      'total_annual_wages' as variable,
      total_annual_wages as value
  FROM qcew_reaggregate_owner

  UNION

  SELECT
      year,
      area_fips,
      own_code,
      industry_code,
      'annual_avg_emplvl' as variable,
      annual_avg_emplvl as value
  FROM qcew_reaggregate_owner
);

CREATE TEMP TABLE qcew AS (
  WITH qcew_piv AS (
    SELECT
        -- encode variable
        CASE
            WHEN variable = 'annual_avg_estabs' THEN 'S'
            WHEN variable = 'avg_annual_pay' THEN 'A'
            WHEN variable = 'total_annual_wages' THEN 'P'
            WHEN variable = 'annual_avg_emplvl' THEN 'M'
        END
        ||
        -- encode county
        CASE 
          WHEN RIGHT(area_fips::TEXT, 3) = '019' THEN 'C'
          WHEN RIGHT(area_fips::TEXT, 3) = '041' THEN 'D'
          WHEN RIGHT(area_fips::TEXT, 3) = '053' THEN 'F'
          WHEN RIGHT(area_fips::TEXT, 3) = '075' THEN 'I'
          WHEN RIGHT(area_fips::TEXT, 3) = '147' THEN 'P'
          WHEN RIGHT(area_fips::TEXT, 3) = '183' THEN 'V'
        END 
        ||
        -- encode industry
        CASE
          WHEN LEFT(industry_code, 2) = '11' THEN '0'
          WHEN LEFT(industry_code, 2) = '21' THEN '1'
          WHEN LEFT(industry_code, 2) = '22' THEN '2'
          WHEN LEFT(industry_code, 2) = '23' THEN '3'
          WHEN LEFT(industry_code, 2) = '31' THEN '4'
          WHEN LEFT(industry_code, 2) = '42' THEN '5'
          WHEN LEFT(industry_code, 2) = '44' THEN '6'
          WHEN LEFT(industry_code, 2) = '48' THEN '7'
          WHEN LEFT(industry_code, 2) = '51' THEN '8'
          WHEN LEFT(industry_code, 2) = '52' THEN '9'
          WHEN LEFT(industry_code, 2) = '53' THEN 'A'
          WHEN LEFT(industry_code, 2) = '54' THEN 'B'
          WHEN LEFT(industry_code, 2) = '55' THEN 'C'
          WHEN LEFT(industry_code, 2) = '56' THEN 'D'
          WHEN LEFT(industry_code, 2) = '61' THEN 'E'
          WHEN LEFT(industry_code, 2) = '62' THEN 'F'
          WHEN LEFT(industry_code, 2) = '71' THEN 'G'
          WHEN LEFT(industry_code, 2) = '72' THEN 'H'
          WHEN LEFT(industry_code, 2) = '81' THEN 'I'
          WHEN LEFT(industry_code, 2) = '92' THEN 'J'
        END
        ||
        -- encode worker type
        'Z'
        ||
        -- encode business type
        CASE
            WHEN own_code = 'A00' THEN 'C'
            WHEN own_code = 'A05' THEN 'P'
        END
        AS variable,
        year,
        value
    FROM qcew_combine
    WHERE value IS NOT NULL
  )
  SELECT
    variable,
    MAX(value) FILTER (WHERE year='2009') AS "2009",
    MAX(value) FILTER (WHERE year='2010') AS "2010",
    MAX(value) FILTER (WHERE year='2011') AS "2011",
    MAX(value) FILTER (WHERE year='2012') AS "2012",
    MAX(value) FILTER (WHERE year='2013') AS "2013",
    MAX(value) FILTER (WHERE year='2014') AS "2014",
    MAX(value) FILTER (WHERE year='2015') AS "2015",
    MAX(value) FILTER (WHERE year='2016') AS "2016",
    MAX(value) FILTER (WHERE year='2017') AS "2017",
    MAX(value) FILTER (WHERE year='2018') AS "2018",
    NULL::FLOAT AS "2019"
  FROM qcew_piv
  WHERE variable IS NOT NULL
  GROUP BY variable
  ORDER BY variable
);

-- QWI
CREATE TEMP TABLE qwi_by_year AS (
  SELECT
      AVG(earns) AS earns,
      AVG(emp) AS emp,
      AVG(frmjbc) AS frmjbc,
      AVG(turnovrs) AS turnovrs,
      AVG(sepsnx) AS sepsnx,
      AVG(hiras) AS hiras,
      AVG(emps) AS emps,
      county,
      ethnicity::TEXT,
      firmsize::TEXT,
      industry::TEXT,
      ownercode::TEXT,
      race::TEXT,
      sex::TEXT,
      state::TEXT,
      year::TEXT
  FROM workforce.qwi
  GROUP BY
      county,
      ethnicity,
      firmsize,
      industry,
      ownercode,
      race,
      sex,
      state,
      year
);


CREATE TEMP TABLE qwi_combine AS (
  SELECT
      earns AS value,
      'earns' AS variable,
      year,
    county,
    industry,
      CASE
          WHEN sex IS NULL AND race IS NULL THEN ethnicity
          WHEN race IS NULL AND ethnicity IS NULL THEN sex
          ELSE race
      END AS worker_type,
      CASE
          WHEN ownercode = 'A05' THEN firmsize
          ELSE ownercode
      END AS business_type
  FROM qwi_by_year

  UNION

  SELECT
      emp AS value,
      'emp' AS variable,
      year,
    county,
    industry,
      CASE
          WHEN sex IS NULL AND race IS NULL THEN ethnicity
          WHEN race IS NULL AND ethnicity IS NULL THEN sex
          ELSE race
      END AS worker_type,
      CASE
          WHEN ownercode = 'A05' THEN firmsize
          ELSE ownercode
      END AS business_type
  FROM qwi_by_year

  UNION

  SELECT
      frmjbc AS value,
      'frmjbc' AS variable,
      year,
    county,
    industry,
      CASE
          WHEN sex IS NULL AND race IS NULL THEN ethnicity
          WHEN race IS NULL AND ethnicity IS NULL THEN sex
          ELSE race
      END AS worker_type,
      CASE
          WHEN ownercode = 'A05' THEN firmsize
          ELSE ownercode
      END AS business_type
  FROM qwi_by_year

  UNION

  SELECT
      turnovrs AS value,
      'turnovrs' AS variable,
      year,
    county,
    industry,
      CASE
          WHEN sex IS NULL AND race IS NULL THEN ethnicity
          WHEN race IS NULL AND ethnicity IS NULL THEN sex
          ELSE race
      END AS worker_type,
      CASE
          WHEN ownercode = 'A05' THEN firmsize
          ELSE ownercode
      END AS business_type
  FROM qwi_by_year

  UNION

  SELECT
      emps AS value,
      'emps' AS variable,
      year,
    county,
    industry,
      CASE
          WHEN sex IS NULL AND race IS NULL THEN ethnicity
          WHEN race IS NULL AND ethnicity IS NULL THEN sex
          ELSE race
      END AS worker_type,
      CASE
          WHEN ownercode = 'A05' THEN firmsize
          ELSE ownercode
      END AS business_type
  FROM qwi_by_year

  UNION 

  SELECT
      sepsnx AS value,
      'sepsnx' AS variable,
      year,
    county,
    industry,
      CASE
          WHEN sex IS NULL AND race IS NULL THEN ethnicity
          WHEN race IS NULL AND ethnicity IS NULL THEN sex
          ELSE race
      END AS worker_type,
      CASE
          WHEN ownercode = 'A05' THEN firmsize
          ELSE ownercode
      END AS business_type
  FROM qwi_by_year

  UNION

  SELECT
      hiras AS value,
      'hiras' AS variable,
      year,
    county,
    industry,
      CASE
          WHEN sex IS NULL AND race IS NULL THEN ethnicity
          WHEN race IS NULL AND ethnicity IS NULL THEN sex
          ELSE race
      END AS worker_type,
      CASE
          WHEN ownercode = 'A05' THEN firmsize
          ELSE ownercode
      END AS business_type
  FROM qwi_by_year
);

CREATE TEMP TABLE qwi AS (
  WITH qwi_piv AS (
    SELECT
      CASE
          WHEN variable = 'frmjbc' THEN 'C'
          WHEN variable = 'emp' THEN 'E'
          WHEN variable = 'earns' THEN 'R'
          WHEN variable = 'turnovrs' THEN 'T'
          WHEN variable = 'emps' THEN 'Q'
          WHEN variable = 'hiras' THEN 'Y'
          WHEN variable = 'sepsnx' THEN 'X'
      END
      ||
      -- encode county
      CASE
        WHEN county = 19 THEN 'C'
        WHEN county = 41 THEN 'D'
        WHEN county = 53 THEN 'F'
        WHEN county = 75 THEN 'I'
        WHEN county = 147 THEN 'P'
        WHEN county = 183 THEN 'V'
      END
      ||
      --  econde industry
      CASE
        WHEN LEFT(industry, 2) = '11' THEN '1'
        WHEN LEFT(industry, 2) = '21' THEN '2'
        WHEN LEFT(industry, 2) = '22' THEN '3'
        WHEN LEFT(industry, 2) = '23' THEN '4'
        WHEN LEFT(industry, 2) = '31' THEN '5'
        WHEN LEFT(industry, 2) = '42' THEN '6'
        WHEN LEFT(industry, 2) = '44' THEN '7'
        WHEN LEFT(industry, 2) = '48' THEN '8'
        WHEN LEFT(industry, 2) = '51' THEN '9'
        WHEN LEFT(industry, 2) = '52' THEN '0'
        WHEN LEFT(industry, 2) = '53' THEN 'A'
        WHEN LEFT(industry, 2) = '54' THEN 'B'
        WHEN LEFT(industry, 2) = '55' THEN 'C'
        WHEN LEFT(industry, 2) = '56' THEN 'D'
        WHEN LEFT(industry, 2) = '61' THEN 'E'
        WHEN LEFT(industry, 2) = '62' THEN 'F'
        WHEN LEFT(industry, 2) = '71' THEN 'G'
        WHEN LEFT(industry, 2) = '72' THEN 'H'
        WHEN LEFT(industry, 2) = '81' THEN 'I'
        WHEN LEFT(industry, 2) = '92' THEN 'J'
      END
      ||
      -- encode worker_type
      CASE
          WHEN worker_type = '1' THEN 'M'
          WHEN worker_type = '2' THEN 'F'
          WHEN worker_type = 'rA1' THEN 'W'
          WHEN worker_type = 'rA2' THEN 'B'
          WHEN worker_type = 'rA3' THEN 'N'
          WHEN worker_type = 'rA4' THEN 'A'
          WHEN worker_type = 'rA5' THEN 'P'
          WHEN worker_type = 'rA7' THEN 'T'
          WHEN worker_type = 'A1' THEN 'K'
          WHEN worker_type = 'A2' THEN 'H'
      END
      ||
      -- encode business_type
      CASE
          WHEN business_type = 'all' THEN 'C'
          WHEN business_type = 'A05' THEN 'P'
          ELSE business_type
      END
      AS variable,
    year,
    value
  FROM qwi_combine
  WHERE value IS NOT NULL
  ORDER BY variable)
  SELECT
    variable,
    MAX(value) FILTER (WHERE year='2009') AS "2009",
    MAX(value) FILTER (WHERE year='2010') AS "2010",
    MAX(value) FILTER (WHERE year='2011') AS "2011",
    MAX(value) FILTER (WHERE year='2012') AS "2012",
    MAX(value) FILTER (WHERE year='2013') AS "2013",
    MAX(value) FILTER (WHERE year='2014') AS "2014",
    MAX(value) FILTER (WHERE year='2015') AS "2015",
    MAX(value) FILTER (WHERE year='2016') AS "2016",
    MAX(value) FILTER (WHERE year='2017') AS "2017",
    MAX(value) FILTER (WHERE year='2018') AS "2018",
    MAX(value) FILTER (WHERE year='2019') AS "2019"
  FROM qwi_piv
  GROUP BY variable
  ORDER BY variable
);

-- SQL for combining the data
CREATE TEMP TABLE combine AS (
  SELECT
    variable,
    ROUND("2009"::numeric, 2) AS "2009",
    ROUND("2010"::numeric, 2) AS "2010",
    ROUND("2011"::numeric, 2) AS "2011",
    ROUND("2012"::numeric, 2) AS "2012",
    ROUND("2013"::numeric, 2) AS "2013",
    ROUND("2014"::numeric, 2) AS "2014",
    ROUND("2015"::numeric, 2) AS "2015",
    ROUND("2016"::numeric, 2) AS "2016",
    ROUND("2017"::numeric, 2) AS "2017",
    ROUND("2018"::numeric, 2) AS "2018",
    ROUND("2019"::numeric, 2) AS "2019"
  FROM qwi

  UNION

  SELECT
    variable,
    ROUND("2009"::numeric, 2) AS "2009",
    ROUND("2010"::numeric, 2) AS "2010",
    ROUND("2011"::numeric, 2) AS "2011",
    ROUND("2012"::numeric, 2) AS "2012",
    ROUND("2013"::numeric, 2) AS "2013",
    ROUND("2014"::numeric, 2) AS "2014",
    ROUND("2015"::numeric, 2) AS "2015",
    ROUND("2016"::numeric, 2) AS "2016",
    ROUND("2017"::numeric, 2) AS "2017",
    ROUND("2018"::numeric, 2) AS "2018",
    ROUND("2019"::numeric, 2) AS "2019"
  FROM laus

  UNION

  SELECT
    variable,
    ROUND("2009"::numeric, 2) AS "2009",
    ROUND("2010"::numeric, 2) AS "2010",
    ROUND("2011"::numeric, 2) AS "2011",
    ROUND("2012"::numeric, 2) AS "2012",
    ROUND("2013"::numeric, 2) AS "2013",
    ROUND("2014"::numeric, 2) AS "2014",
    ROUND("2015"::numeric, 2) AS "2015",
    ROUND("2016"::numeric, 2) AS "2016",
    ROUND("2017"::numeric, 2) AS "2017",
    ROUND("2018"::numeric, 2) AS "2018",
    ROUND("2019"::numeric, 2) AS "2019"
  FROM acs

  UNION

  SELECT
    variable,
    ROUND("2009"::numeric, 2) AS "2009",
    ROUND("2010"::numeric, 2) AS "2010",
    ROUND("2011"::numeric, 2) AS "2011",
    ROUND("2012"::numeric, 2) AS "2012",
    ROUND("2013"::numeric, 2) AS "2013",
    ROUND("2014"::numeric, 2) AS "2014",
    ROUND("2015"::numeric, 2) AS "2015",
    ROUND("2016"::numeric, 2) AS "2016",
    ROUND("2017"::numeric, 2) AS "2017",
    ROUND("2018"::numeric, 2) AS "2018",
    ROUND("2019"::numeric, 2) AS "2019"
  FROM qcew
);

-- Export table to CSV
COPY (
  SELECT * FROM combine 
) TO '/tmp/combine.csv' CSV HEADER;
