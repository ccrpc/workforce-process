import time
import warnings
import pandas as pd
from sqlalchemy import create_engine
import urllib.request
import urllib.parse

from key import API_KEY

warnings.filterwarnings("ignore")
pd.set_option("display.max_rows", 500)


engine = create_engine(
    "{}://{}:{}@{}:{}/{}".format(
        "postgresql", "admin", "password", "localhost", "5432", "db",
    )
)

# Utilitize classes for connecting to the Census REST API


class API:
    """
    Baseclass for creating the Census class, this class has the template for
    downloading constructing HTTP query and downloading the data.
    """
    def __init__(self, key, baseUrl):
        """
        Constructor for the class

        Arguments:
        key -- string - API key for accessing the API
        baseUrl -- string - Base Url end point for the API
        """
        self.key = key
        self.baseUrl = baseUrl
        self.query = ""

    def construct_query(self):
        """
        Abstract method that needs to be implement by a child class
        """
        raise NotImplementedError("Implemnt by child class")

    def download_data(self, verbose=False):
        """
        Download data based on query constructed

        Keyword arguments:
        verbose -- boolean
        """
        self.construct_query(verbose)
        request = urllib.request.Request(self.query)
        return urllib.request.urlopen(request)


class Census(API):
    def __init__(self, key, baseUrl="https://api.census.gov/data"):
        """
        Constructor for the class

        Arguments:
        key -- string - API key for accessing the API
        baseUrl -- string - Base Url end point for the API
        year -- string - data year
        fields -- list - list of indicator to download
        filter -- list - list of filter parameter to apply to the query
        """
        super().__init__(key, baseUrl)
        self.year = ""
        self.survey = ""
        self.fields = []
        self.filter = []

    def get_API_KEY(self):
        """ Print API key """
        print(self.key)

    def set_year(self, year):
        """
        Set the year for download

        Arguments:
        year -- integer / string
        """
        self.year = str(year)

    def set_survey(self, survey):
        """
        Set the survey for download

        Arguments:
        survey -- string
        """
        self.survey = str(survey)

    def set_field(self, fields):
        """
        Set the fields for download

        Arguments:
        fields -- list (example: ['emp', 'year'])
        """
        self._checkIsList(fields)
        self.fields = []
        for field in fields:
            self.fields.append(field)

    def set_geoFilter(self, filter):
        """
        Set the filters to apply to download

        Arguments:
        filter -- list (example: [in=state:17]), see censsu for how filters 
            can be used.
        """
        self._checkIsList(filter)
        if type(filter) is not list:
            raise TypeError
        self.filter = []
        for criteria in filter:
            self.filter.append(criteria)

    def construct_query(self, verbose=False):
        """
        Construct query based on the object project

        Keyword Arguments:
        verbose -- boolean
        """
        fields = ",".join(self.fields)
        criteria = "&".join(self.filter)
        queryString = "/{}/{}?get={}&{}&key={}".format(
            self.year, self.survey, fields, criteria, self.key
        )
        self.query = self.baseUrl + queryString
        if verbose:
            print(self.query)

    def download_data(self, verbose=False):
        """
        Download the data

        Keyword Arguments:
        verbose -- boolean
        """
        self._checkIsEmpty()
        self.construct_query(verbose)
        request = urllib.request.Request(self.query)
        response = urllib.request.urlopen(request)
        return pd.read_json(response, orient="values")

    def _checkIsEmpty(self):
        """
        Helper method to check the validity of the object
        """
        if self.year == "":
            raise ValueError("Year has not been set")
        elif self.survey == "":
            raise ValueError("Survey has not been set")
        elif self.fields == []:
            raise ValueError("Fields have not been set")
        elif self.set_geoFilter == []:
            raise ValueError("Geofilter has not been set")

    def _checkIsList(self, input):
        """
        Helper method to check the validity of the input
        """
        if type(input) is not list:
            raise TypeError(
                "The input of {} must be in a list format".format(str(input))
            )


# Utilitize functions for downloading and transforming the data
def delay(seconds, message=None):
    """
    Add delay between API query

    Argument:
    seconds -- integer, how long is the delay

    Keyword Argument:
    message -- string, custom message to display during the wait period
    """
    if message:
        print(message)
    for i in range(seconds):
        time.sleep(1)
    print("---------")


def process(df):
    """
    Helper function that apply cleaning process and force numeric string data
    into numeric type
    """
    df = clean_data(df)
    df = df.apply(pd.to_numeric, errors="ignore")
    return df


def clean_data(df):
    """ Use the first row to column header """
    df = df.rename(df.loc[0], axis="columns")
    df = df.iloc[1:]
    return df


def download_data(survey, census, variable, filters):
    """ Helper function for downloading the data """
    census.set_survey(survey)
    census.set_field([variable, "year"])
    census.set_geoFilter(filters)
    df = census.download_data(True)
    return df


def get_household_size():
    """
    Main function to set the parameter to the census object and donwload data
    for household size.
    """
    ndf = pd.DataFrame()
    for year in YEARS:
        delay(1)
        census = Census(API_KEY)
        census.set_year(str(year))
        census.set_survey("acs/acs5")
        census.set_field(VARIABLES)
        census.set_geoFilter([COUNTY_PARAM, STATE_PARAM])
        df = process(census.download_data(True))
        df["year"] = str(year)
        ndf = pd.concat([ndf, df])
    return ndf


def get_household_income():
    """
    Main function to set the parameter to the census object and donwload data
    for household income.
    """
    ndf = pd.DataFrame()
    for year in YEARS:
        delay(1)
        census = Census(API_KEY)
        census.set_year(str(year))
        census.set_survey("acs/acs5")
        census.set_field(VARIABLES)
        census.set_geoFilter([COUNTY_PARAM, STATE_PARAM])
        df = process(census.download_data(True))
        df["year"] = str(year)
        ndf = pd.concat([ndf, df])
    return ndf


if __name__ == "__main__":
    """
    The followings are paramters needed to construct the HTTP query to get the
    data from the Census REST API
    """
    COUNTIES = ["019", "041", "053", "075", "147", "183"]
    COUNTY_PARAM = f"for=county:{','.join(COUNTIES)}"
    STATE_PARAM = f"in=state:17"
    YEARS = [2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018]

    """ Get household size """
    VARIABLES = ["B10063_001E"]
    MAP = {
        "B10063_001E": "total_household",
    }
    household_size = get_household_size().rename(columns=MAP)
    household_size.columns = map(str.lower, household_size.columns)
    household_size = household_size.set_index(["state", "county", "year"])

    """ Get median income """
    VARIABLES = ["B19019_001E"]
    MAP = {
        "B19019_001E": "median_income",
    }
    income = get_household_income().rename(columns=MAP)
    income.columns = map(str.lower, income.columns)
    income = income.set_index(["state", "county", "year"])

    """ Merge the two dataframe and import it to the database """
    combined = household_size.merge(
        income, left_index=True, right_index=True
    ).reset_index()
    combined.to_sql("acs", engine, "workforce", if_exists="replace")
