import os
import pandas as pd

DB_PATH = os.path.join('db', 'labor_shed_survey.db')

CAMPAIGNS = {
    'ILLINOIS_CHAMPAIGN': 'Champaign',
    'ILLINOIS_FORD': 'Ford',
    'ILLINOIS_IROQUOIS': 'Iroquois',
    'ILLINOIS_PIATT': 'Piatt',
    'ILLINOIS_DOUGLAS': 'Douglas',
    'ILLINOIS_VERMILION': 'Vermilion'
}


# categorized example
# {
#     'type': 'categorized',
#     'number': '1',
#     'output': 'question_1.csv',
#     'sum': False
# }

# graduated example
# {
#     'type': 'graduated',
#     'number': '4',
#     'output': 'question_4.csv',
#     'index':  pd.IntervalIndex.from_arrays(
#         [-float('Inf'), 10, 20, 30, 40, 50, 60, 70, 80, 90],
#         [10, 20, 30, 40, 50, 60, 70, 80, 90, float('Inf')]),
#     'label': 'Average Work Hours (Wk)',
#     'label_categories': ['0 - 10', '10 - 20', '20 - 30', '30 - 40',
#                          '40 - 50', '50 - 60', '60 - 70', '70 - 80',
#                          '80 - 90', '90+'],
#     'sum': False
# }
QUESTIONS = [
    {
        'type': 'categorized',
        'number': '1',
        'output': 'question_1.csv',
    },
    {
        'type': 'categorized',
        'number': '1A',
        'output': 'question_1A.csv',
    },
    {
        'type': 'graduated',
        'number': '4',
        'output': 'question_4.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 10, 20, 30, 40, 50, 60, 70, 80, 90],
            [10, 20, 30, 40, 50, 60, 70, 80, 90, float('Inf')]),
        'label': 'Average Work Hours (Wk)',
        'label_categories': ['0 - 10', '10 - 20', '20 - 30', '30 - 40',
                             '40 - 50', '50 - 60', '60 - 70', '70 - 80',
                             '80 - 90', '90+'],
    },
    {
        'type': 'graduated',
        'number': '6',
        'output': 'question_6.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 1, 2, 5, 10, 20, 40, 60],
            [1, 2, 5, 10, 20, 40, 60, float('Inf')]),
        'label': 'Average Work Commute (One-Way in Miles)',
        'label_categories': ['0-1', '1-2', '2-5', '5-10', '10-20',
                             '20-40', '40-60', '60+'],
    },
    {
        'type': 'categorized',
        'number': '7',
        'output': 'question_7.csv',
    },
    {
        'type': 'graduated',
        'number': '9',
        'output': 'question_9.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 1, 5, 10, 20],
            [1, 5, 10, 20, float('Inf')]),
        'label': 'Number of Years at Current Job',
        'label_categories': ['0-1', '1-5', '5-10', '10-20', '20+'],
    },
    {
        'type': 'graduated',
        'number': '10',
        'output': 'question_10.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 10, 20, 30, 40, 50],
            [10, 20, 30, 40, 50, float('Inf')]),
        'label': 'Years of Experience in Current Field of Work',
        'label_categories': ['0-10', '10-20', '20-30',
                             '30-40', '40-50', '50+'],
    },
    {
        'type': 'categorized',
        'number': '11',
        'output': 'question_11.csv',
    },
    {
        'type': 'categorized',
        'number': '12',
        'output': 'question_12.csv',
    },
    {
        'type': 'categorized',
        'number': '13',
        'output': 'question_13.csv',
    },
    {
        'type': 'categorized',
        'number': '14',
        'output': 'question_14.csv',
    },
    {
        'type': 'categorized',
        'number': '15',
        'output': 'question_15.csv',
    },
    {
        'type': 'categorized',
        'number': '17',
        'output': 'question_17.csv',
    },
    {
        'type': 'categorized',
        'number': '18',
        'output': 'question_18.csv',
    },
    {
        'type': 'categorized',
        'number': '19',
        'output': 'question_19.csv',
    },
    {
        'type': 'categorized',
        'number': '19A',
        'output': 'question_19A.csv',
    },
    {
        'type': 'categorized',
        'number': '19B',
        'output': 'question_19B.csv',
    },
    {
        'type': 'categorized',
        'number': '19C',
        'output': 'question_19C.csv',
    },
    {
        'type': 'categorized',
        'number': '20',
        'output': 'question_20.csv',
    },
    {
        'type': 'graduated',
        'number': '21',
        'output': 'question_21.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 5, 10, 20, 40, 60],
            [5, 10, 20, 40, 60, float('Inf')]),
        'label': 'Max One Way Commute Time (In Minutes)',
        'label_categories': ['0-5', '5-10', '11-20', '21-40', '41-60', '60+'],
    },
    {
        'type': 'categorized',
        'number': '22',
        'output': 'question_22.csv',
    },
    {
        'type': 'categorized',
        'number': '23',
        'output': 'question_23.csv',
    },
    {
        'type': 'categorized',
        'number': '24',
        'output': 'question_24.csv',
    },
    {
        'type': 'categorized',
        'number': '25',
        'output': 'question_25.csv',
    },
    {
        'type': 'categorized',
        'number': '26',
        'output': 'question_26.csv',
    },
    {
        'type': 'categorized',
        'number': '27',
        'output': 'question_27.csv',
    },
    {
        'type': 'categorized',
        'number': '28',
        'output': 'question_28.csv',
    },
    {
        'type': 'categorized',
        'number': '29',
        'output': 'question_29.csv',
    },
    {
        'type': 'categorized',
        'number': '30',
        'output': 'question_30.csv',
    },
    {
        'type': 'categorized',
        'number': '32',
        'output': 'question_32.csv',
    },
    {
        'type': 'categorized',
        'number': '33',
        'output': 'question_33.csv',
    },
    {
        'type': 'graduated',
        'number': '34',
        'output': 'question_34.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 6, 12, 24, 60, 120, 240],
            [6, 12, 24, 60, 120, 240, float('Inf')]),
        'label': 'How Long Since Last Employed',
        'label_categories': ['0-6 months', '7-12 months', '1-2 years',
                             '3-5 years', '6-10 years', '11-20 years',
                             '20+ years'],
    },
    {
        'type': 'categorized',
        'number': '36',
        'output': 'question_36.csv',
    },
    {
        'type': 'graduated',
        'number': '37',
        'output': 'question_37.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 9, 15, 20, 40, 60, 500,
             30000, 50000, 80000, 100000, 150000],
            [9, 15, 20, 40, 60, 500, 30000, 50000,
             80000, 100000, 150000, float('Inf')]),
        'label': 'Hourly Wage or Annual Salary From Last Job',
        'label_categories': ['$9/hr or less', '$10-15/hr', '$16-20/hr',
                             '$21-40/hr', '$41-60/hr', '$60+/hr',
                             '$30,000/yr or less', '$31,000-50,000/yr',
                             '$51,000-80,000/yr', '$81,000-100,000/yr',
                             '$101,000-150,000/yr', '$150,001+/yr'],
    },
    {
        'type': 'graduated',
        'number': '38',
        'output': 'question_38.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 5, 10, 20, 40, 60],
            [5, 10, 20, 40, 60, float('Inf')]),
        'label':
            'Maximum One-Way Commute Time for New Employment (In Minutes)',
        'label_categories': ['0-5', '6-10', '11-20', '21-40', '41-60', '60+'],
    },
    {
        'type': 'categorized',
        'number': '39',
        'output': 'question_39.csv',
    },
    {
        'type': 'graduated',
        'number': '40',
        'output': 'question_40.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 9, 15, 20, 40, 60, 500, 30000,
             50000, 80000, 100000, 150000],
            [9, 15, 20, 40, 60, 500, 30000, 50000, 80000,
             100000, 150000, float('Inf')]),
        'label':
            'Minimum Hourly Wage or Salary Required',
        'label_categories': ['$9/hr or less', '$10-15/hr', '$16-20/hr',
                             '$21-40/hr', '$41-60/hr', '$60+/hr',
                             '$30,000/yr or less', '$31,000-50,000/yr',
                             '$51,000-80,000/yr', '$81,000-100,000/yr',
                             '$101,000-150,000/yr', '$150,001+/yr'],
    },
    {
        'type': 'categorized',
        'number': '42',
        'output': 'question_42.csv',
    },
    {
        'type': 'categorized',
        'number': '43',
        'output': 'question_43.csv',
    },
    {
        'type': 'categorized',
        'number': '44',
        'output': 'question_44.csv',
    },
    {
        'type': 'categorized',
        'number': '45',
        'output': 'question_45.csv',
    },
    {
        'type': 'categorized',
        'number': '46',
        'output': 'question_46.csv',
    },
    {
        'type': 'categorized',
        'number': '48',
        'output': 'question_48.csv',
    },
    {
        'type': 'categorized',
        'number': '49A',
        'output': 'question_49A.csv',
    },
    {
        'type': 'categorized',
        'number': '49B',
        'output': 'question_49B.csv',
    },
    {
        'type': 'categorized',
        'number': '49C',
        'output': 'question_49C.csv',
    },
    {
        'type': 'categorized',
        'number': '50',
        'output': 'question_50.csv',
    },
    {
        'type': 'categorized',
        'number': '51',
        'output': 'question_51.csv',
    },
    {
        'type': 'categorized',
        'number': '53',
        'output': 'question_53.csv',
    },
    {
        'type': 'categorized',
        'number': '55',
        'output': 'question_55.csv',
    },
    {
        'type': 'categorized',
        'number': '56',
        'output': 'question_56.csv',
    },
    {
        'type': 'graduated',
        'number': '57',
        'output': 'question_57.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 1, 2, 5, 10],
            [1, 2, 5, 10, float('Inf')]),
        'label':
            'Years Since Last Employed',
        'label_categories': ['0-1', '1-2', '3-5', '5-10', '10+'],
    },
    {
        'type': 'categorized',
        'number': '59',
        'output': 'question_59.csv',
    },
    {
        'type': 'graduated',
        'number': '60',
        'output': 'question_60.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 9, 15, 20, 40, 60, 500, 30000,
             50000, 80000, 100000, 150000],
            [9, 15, 20, 40, 60, 500, 30000, 50000, 80000,
             100000, 150000, float('Inf')]),
        'label':
            'Hourly Wage or Annual Salary of You Last Job',
        'label_categories': ['$9/hr or less', '$10-15/hr', '$16-20/hr',
                             '$21-40/hr', '$41-60/hr', '$60+/hr',
                             '$30,000/yr or less', '$31,000-50,000/yr',
                             '$51,000-80,000/yr', '$81,000-100,000/yr',
                             '$101,000-150,000/yr', '$150,001+/yr'],
    },
    {
        'type': 'graduated',
        'number': '61',
        'output': 'question_61.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 5, 10, 20, 40, 60],
            [5, 10, 20, 40, 60, float('Inf')]),
        'label':
            'Maximum One-Way Commute Time for New Employment (In Minutes)',
        'label_categories': ['0-5', '6-10', '11-20', '21-40', '41-60', '60+'],
    },
    {
        'type': 'categorized',
        'number': '62',
        'output': 'question_62.csv',
    },
    {
        'type': 'graduated',
        'number': '63',
        'output': 'question_63.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 9, 15, 20, 40, 60, 500, 30000,
             50000, 80000, 100000, 150000],
            [9, 15, 20, 40, 60, 500, 30000, 50000, 80000,
             100000, 150000, float('Inf')]),
        'label':
            'Minimum Hourly Wage or Annual Required',
        'label_categories': ['$9/hr or less', '$10-15/hr', '$16-20/hr',
                             '$21-40/hr', '$41-60/hr', '$60+/hr',
                             '$30,000/yr or less', '$31,000-50,000/yr',
                             '$51,000-80,000/yr', '$81,000-100,000/yr',
                             '$101,000-150,000/yr', '$150,001+/yr'],
    },
    {
        'type': 'categorized',
        'number': '65',
        'output': 'question_65.csv',
    },
    {
        'type': 'categorized',
        'number': '66',
        'output': 'question_66.csv',
    },
    {
        'type': 'categorized',
        'number': '67',
        'output': 'question_67.csv',
    },
    {
        'type': 'categorized',
        'number': '69',
        'output': 'question_69.csv',
    },
    {
        'type': 'categorized',
        'number': '71',
        'output': 'question_71.csv',
    },
    {
        'type': 'categorized',
        'number': '72',
        'output': 'question_72.csv',
    },
    {
        'type': 'categorized',
        'number': '72A',
        'output': 'question_72A.csv',
    },
    {
        'type': 'categorized',
        'number': '72B',
        'output': 'question_72B.csv',
    },
    {
        'type': 'categorized',
        'number': '72C',
        'output': 'question_72C.csv',
    },
    {
        'type': 'categorized',
        'number': '73',
        'output': 'question_73.csv',
    },
    {
        'type': 'graduated',
        'number': '74',
        'output': 'question_74.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 1, 5, 10, 20, 40],
            [1, 5, 10, 20, 40, float('Inf')]),
        'label':
            'Years Lived in Area',
        'label_categories': ['0-1', '2-5', '6-10', '11-20', '21-40', '41+'],
    },
    {
        'type': 'categorized',
        'number': '75',
        'output': 'question_75.csv',
    },
    {
        'type': 'categorized',
        'number': '76',
        'output': 'question_76.csv',
    },
    {
        'type': 'graduated',
        'number': '77',
        'output': 'question_77.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 1, 2, 3],
            [1, 2, 3, float('Inf')]),
        'label':
            'Household Distribution',
        'label_categories': ['1', '2', '3', '4+'],
    },
    {
        'type': 'categorized',
        'number': '78',
        'output': 'question_78.csv',
    },
    {
        'type': 'categorized',
        'number': '79',
        'output': 'question_79.csv',
    },
    {
        'type': 'graduated',
        'number': '80',
        'output': 'question_80.csv',
        'index':  pd.IntervalIndex.from_arrays(
            [-float('Inf'), 20, 30, 40, 50, 60, 70, 80, 90],
            [20, 30, 40, 50, 60, 70, 80, 90, float('Inf')]),
        'label':
            'Age',
        'label_categories': ['0-20', '21-30', '31-40', '41-50', '51-60',
                             '61-70', '71-80', '81-90', '90+'],
    },
    {
        'type': 'categorized',
        'number': '81',
        'output': 'question_81.csv',
    },
    {
        'type': 'categorized',
        'number': '82',
        'output': 'question_82.csv',
    },
]
