import psycopg2
import pandas as pd
import geopandas as geopd
from sqlalchemy import create_engine


class dbUtil:
    @staticmethod
    def read_postgis(conn, sql):
        with psycopg2.connect(**conn) as conn:
            try:
                df = pd.read_sql_query(sql, conn)
            except pd.io.sql.DatabaseError:
                table = sql.split('FROM ')
                print('{} table does not exist...'.format(table[1]))
                df = None
            return df

    @staticmethod
    def read_gdb(path, layer):
        return geopd.read_file(path, driver='FileGDB', layer=layer)

    @staticmethod
    def read_shapefile(path):
        return geopd.read_file(path)

    @staticmethod
    def read_csv(path):
        return pd.read_csv(path)

    @staticmethod
    def write_geopackage(pd, path):
        return pd.to_file(filename=path, driver='OpenFileGDB')

    @staticmethod
    def write_postgis(df, table_name, conn, schema, port='5432'):
        engine = create_engine('{}://{}:{}@{}:{}/{}'.format(
            'postgresql',
            conn.get('user'),
            conn.get('password'),
            conn.get('host'),
            port,
            conn.get('dbname'),
        ))
        df.to_sql(table_name, engine, schema, if_exists='append')

    @staticmethod
    def join_df(odf, ndf, odf_match_col, ndf_match_col, merge_type='left'):
        return pd.merge(
            odf,
            ndf,
            how=merge_type,
            left_on=odf_match_col,
            right_on=ndf_match_col,
        )

    @staticmethod
    def filter_df(df, filter_col):
        return df[filter_col]

    @staticmethod
    def rename_column(df, col_dict):
        return df.rename(columns=col_dict)

    @staticmethod
    def convert_data_type(df, column, type):
        if type == 'numeric':
            df[column] = pd.to_numeric(df[column])
        elif type == 'string':
            df[column] = df[column].astype(str)
        elif type == 'whole':
            df[column] = df[column].map("{:.0f}".format)
        else:
            raise ValueError('type is not correct')
        return df

    @staticmethod
    def compare_df(odf, ndf):
        rdf = pd.DataFrame(columns=odf.columns.tolist())
        for row in ndf.iterrows():
            data = row[1]
            if ((odf['segment_id'] == data['segment_id']) &
               (odf['year'] == data['year'])).any():
                continue
            else:
                rdf = rdf.append(data)
        return rdf

    @staticmethod
    def groupby_df(df, column, groupby_type='mean'):
        if groupby_type == 'mean':
            return df.groupby(column).mean()

    @staticmethod
    def set_df_index(df, columns):
        return df.set_index(columns)

    @staticmethod
    def remove_row_value(df, column, row_value):
        return df.loc[df[column] != row_value]

    @staticmethod
    def remove_null(df, column):
        return df.loc[df[column].notnull()]
