# Employee Survey
This directory contains the script for processing the employee survey data.

Contains scripts for data processing for the workforce data portal. **NOTICE** this repository only contain part of the scripts that transform the preprocess data. The scripts read the data out of a sqlite data base, apply transformation, then output questions as CSV for further visualization or publication.

## Files
### dbUtil.py
Contains general utility functions that wraps around pandas dataFrame to access frenquently use pandas functions, such as connecting to the database, remove columns, setting index, converting between datatype, aggreating dataframes, etc.

### helper.py
This file contains helper functions specifically for transforming data to this purpose.  It contains SQL syntax used to query data from the database and pandas transformation function.   

### config.py
This scripts set set up parameters needed by process.py to transform each questions based on the question type and how to bin the value into categories or range.

*DB_PATH* - String: Set the path to the SQLite database.
*CAMPAIGNS* - Dictionary: Set the different campaigns that occur in each county. The key is the value of the in the database, the value is the santized label for each campaign.
*QUESTIONS* - List of dictionary: This configuration list tells how the process algorithm how to process each question based on rather its categorized or graduated data type.  It also supplies other variables such as output, indexing, labeling, and label category.  See below for how to construct this array.

Categorized example
```python
{
    'type': 'categorized',
    'number': '1',
    'output': 'question_1.csv',
    'sum': False
}
```

Graduated example
```python
{
    'type': 'graduated',
    'number': '4',
    'output': 'question_4.csv',
    'index':  pd.IntervalIndex.from_arrays(
        [-float('Inf'), 10, 20, 30, 40, 50, 60, 70, 80, 90],
        [10, 20, 30, 40, 50, 60, 70, 80, 90, float('Inf')]),
    'label': 'Average Work Hours (Wk)',
    'label_categories': ['0 - 10', '10 - 20', '20 - 30', '30 - 40',
                         '40 - 50', '50 - 60', '60 - 70', '70 - 80',
                         '80 - 90', '90+'],
    'sum': False
}
```

### process.py
This file contains functions that read the database processing the data using the configuration files and outputting the data to a specify location. 