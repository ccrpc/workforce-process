from helper import get_categorize_table, get_graduated_table, sum_table, \
    convert_to_perentage
from config import QUESTIONS, CAMPAIGNS
from dbUtil import dbUtil
import os


def to_int(df, number):
    for co in CAMPAIGNS.keys():
        try:
            df = dbUtil.convert_data_type(df, co, 'whole')
        except KeyError:
            print(f'{co} does not have data in table {number}')
    return df


def main(percentage=False):
    for q in QUESTIONS:
        if q['type'] == 'categorized':
            df = get_categorize_table(q['number'])
        elif q['type'] == 'graduated':
            df = get_graduated_table(q['number'], q['index'])
            df.insert(0, q['label'], q['label_categories'])

        if q.get('sum'):
            df = sum_table(df)

        df = df.fillna(0)
        if percentage:
            df = convert_to_perentage(df)
        df = to_int(df, q['number'])
        df = dbUtil.rename_column(df, CAMPAIGNS)
        df = df.reset_index()
        df.to_csv(os.path.join('output', q['output']), index=False)


if __name__ == '__main__':
    main(percentage=True)
