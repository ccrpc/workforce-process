from config import DB_PATH, CAMPAIGNS
import sqlite3 as sql
from dbUtil import dbUtil
import pandas as pd


def sum_table(df, label='Total'):
    df[label] = df.sum(axis=1)
    return df


def average_table(df, label='Average'):
    df[label] = df.mean(axis=1)
    return df


CATEGORIZED_SQL = """
    SELECT a.answer_desc, count(a.answer_desc) AS count FROM answers AS a
    JOIN calls AS c ON a.client_no = c.client_no
    WHERE a.question_num = "{}" AND c.campaign = "{}"
    GROUP BY c.campaign, a.answer_desc
"""


def get_categorize_table(question_number):
    conn = sql.connect(DB_PATH)
    df = pd.DataFrame()
    for campaign in CAMPAIGNS.keys():
        ndf = pd.read_sql_query(
            CATEGORIZED_SQL.format(question_number, campaign), conn)
        ndf = dbUtil.rename_column(
            ndf,
            {'answer_desc': 'Answer', 'count': campaign})
        if df.empty:
            df = ndf
            continue
        df = dbUtil.join_df(df, ndf, 'Answer', 'Answer', 'outer')
    return df


GRAUDATED_SQL = """SELECT a.answer AS count FROM answers AS a
    JOIN calls AS c ON a.client_no = c.client_no
    WHERE a.question_num = "{}" AND c.campaign = "{}"
"""


def get_graduated_table(question_number, index):
    conn = sql.connect(DB_PATH)
    df = pd.DataFrame()
    for campaign in CAMPAIGNS.keys():
        ndf = pd.read_sql_query(
            GRAUDATED_SQL.format(question_number, campaign), conn)
        # bin value
        ndf['group'] = pd.cut(pd.to_numeric(ndf['count']), index, right=False)
        ndf = ndf.groupby('group').count()
        ndf = dbUtil.rename_column(ndf, {'count': campaign})
        if df.empty:
            df = ndf
            continue
        df = dbUtil.join_df(df, ndf, 'group', 'group')
    return df


def convert_to_perentage(df):
    df = df.set_index(df.columns[0])
    df = (100. * df / df.sum()).round(0)
    return df
