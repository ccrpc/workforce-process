# Workforce-process
This repostiory contains data extraction and transformation scripts.  The specific documentations for each part are located in the specifc folder. 

## How to update data for workforce data portal

In `src/data-seeker/`, contains python scripts for all the different resources.  The general idea is to first create a local Postgres database, then the script will download the data from either the data resource's API or files.  Do some minimal cleaning, then import it into the Postgres database.  Then using SQL, perform further cleaning and transformation.  Output it as a CSV file.  Then run a python script that perform aggregation to arrived at the final output that can be used by the data portal.  

To update the data, we will probably need to download all the data from scratch.  Python dependencies are:

- sqlalchemy
- pandas
- request
- urllib
- time
- warnings
- numpy 
- re
- csv
- BeautifulSoup
- datetime

#### Downloading the data
Make sure to change the local database information in each of the following files so that it is pointing to the database that you spin up.


**acs_api.py**
The parameter that needs to be changed is the YEAR array to include the latest year.   

**laus_dumpy.py**
Change the year range, to include the newest years.  The script will work unless the underlying url changes from their website. 

**qcew_dump.py**
Like the other script changes the year range will get the most up to update data.

**qwi_api.py**
Changing this line to the latest year and quarter should also work.  If the API remain unchanged.
```
YEAR_PARAM = f"time=from2009-Q1to2019-Q4"
```

**warn.py**
Changing the CSV_PATH const to point to the new WARN data

#### SQL
After all the data is in the database You can run the `union.sql` that combines all the data source into a single CSV file into a standard format.

#### aggreation.py
With the single CSV.  Run the `aggregation.py` files to aggregation form of the data.  